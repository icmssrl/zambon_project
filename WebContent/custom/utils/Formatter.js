jQuery.sap.declare("utils.Formatter");
jQuery.sap.require("utils.ParseDate");

utils.Formatter = {

    formatPriority :  function (p) {
        if(p){
//            p.substring(3);
            if(p === "4: Low"){
                return "Success";
            }else if (p === "3: Medium") {
				return "Warning";
			} else if (p === "2: High"){
				return "Warning";
			}else if (p === "1: Very High"){
				return "Error";
			} else {
				return "None";
			}
        }
        
//			if (p === "Normal") {
//				return "Success";
//			} else if (p === "Warning") {
//				return "Warning";
//			} else if (p === "Urgent"){
//				return "Error";
//			} else {
//				return "None";
//			}
    },
    
    isSelected: function (obj) {
        if (obj !== "")
          return true;
        return false;

    },
    
    deleteOnEditMode: function (val){
        if(val !== undefined && val !== null && val !== ""){
            if(val === true){
                return "Delete";
            }else{
                return "None";
            }
        }else{
            return "None";
        }
    },
    
    parseTzDate: function(val){
        if(val === "" || val === undefined || val === null ){
            return "";
        }else{
            if(val.length> 0){
                var year = val.substring(0,4);
                var month = val.substring(4,6);
                var day = val.substring(6,8);
                
                return day +"-"+ month +"-"+ year;
            }
            
        }
    },
    
    timeToMs: function(s) {
          if(s === 0 || s === "" || s === undefined || s === null ){
              return "";
          }else{
              var ms = s % 1000;
              s = (s - ms) / 1000;
              var secs = s % 60;
              s = (s - secs) / 60;
              var mins = s % 60;
              var hrs = (s - mins) / 60;
              if(hrs<10)
                  hrs="0"+hrs;
              if(mins<10)
                  mins="0"+mins;
              return hrs + ':' + mins + ':' + secs ;
          }
          

     },
    
    formatEnToListType: function(enable)
    {
        return (enable)? "Delete":"None";
    },
    
    formatDate: function(value) {
        if (value) {
        return sap.ui.core.format.DateFormat.getDateInstance({pattern: "MM-dd-yyyy"}).format(value);                         
        }
        else {
            return null;
        }
    },
    
    formatDateFromService: function(value) {
        var date = new Date(value);
        if (date) {
        // return sap.ui.core.format.DateFormat.getDateInstance({pattern: "MM-dd-yyyy"}).format(date);          
         return sap.ui.core.format.DateFormat.getDateInstance({pattern: "dd-MM-yyyy"}).format(date);         
        }
        else {
            return null;
        }
    },



};
