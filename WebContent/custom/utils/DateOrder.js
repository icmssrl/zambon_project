jQuery.sap.declare("utils.DateOrder");

utils.DateOrder=
{
    //this function divide the date in its component and return an array
  divideDate:function(date)
  {
    var year, month, day;
    if(date=="")
    {
      year=0;
      month=0;
      day=0;
    }
    else {
	  var separator = /[-/]/;
      //var arrayDate= date.split("-");
      var arrayDate= date.split(separator);
// f = new Date(from[2], from[1] - 1, from[0]);
      year = parseInt(arrayDate[2]);
      month = parseInt(arrayDate[1]);
      day= parseInt(arrayDate[0]);
    }

    var dateObj = [day, month, year];
    return dateObj;
  },
  
  compareDate : function(firstDate, secondDate)
  {
    if ((firstDate== null || firstDate == undefined || firstDate == '') && (secondDate == null || secondDate == undefined || secondDate== ''))
      return 0;

    //divide dates in their parts: year, month, day
    var value1= this.divideDate(firstDate);
    var value2= this.divideDate(secondDate);
    for(var i=value1.length-1; i>=0; i--)
    {
      if(!(parseInt(value1[i])==parseInt(value2[i])))
      {
        return (parseInt(value1[i])<parseInt(value2[i])) ? -1 : 1;
      }
    }
    return 0;
  }



}