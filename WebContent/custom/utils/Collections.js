jQuery.sap.declare("utils.Collections");
jQuery.sap.require("model.persistence.Serializer");

utils.Collections = {

    _defer : {},
    _collectionsModel : {},

      getById : function (collections, idCode)
      {
        return _.find(collections, {id:idCode});
      },
      getModel : function(collectionName)
      {
        if(this._defer[collectionName] && this._defer[collectionName].promise.isFulfilled())
        {
          this._defer[collectionName].resolve(this._collectionsModel[collectionName]);
        }
        else
        {
          this._defer[collectionName] = Q.defer();
            var fSuccess = function(result)
            {
              this._collectionsModel[collectionName] = new sap.ui.model.json.JSONModel(result);

              this._defer[collectionName].resolve(this._collectionsModel[collectionName]);
            }
            fSuccess = _.bind(fSuccess, this);

            var fError = function(err)
            {
              this._collectionsModel[collectionName]={};
              this._defer[collectionName].reject(err);
            }
            fError = _.bind(fError, this);
            //It wil be trasformed to the respective odata call
            $.getJSON("custom/model/mock/data/"+collectionName+".json")
              .success(fSuccess)
              .fail(fError);
        }
        return this._defer[collectionName].promise;
      },
    
//      getPrioritySelect: function(set)
//      {
//        var defer = Q.defer();
//        var fSuccess = function(result)
//        {
//          console.log("value help items loaded");
//          var model  = new sap.ui.model.json.JSONModel(result);
//          defer.resolve(model);
//        };
//        fSuccess = _.bind(fSuccess, this);
//
//        var fError = function(err)
//        {
//          console.log("Error Loading value help Items");
//          defer.reject(err);
//        }
//        fError = _.bind(fError, this);
//
//        model.odata.chiamateOdata.getPrioritySelect(set,fSuccess, fError);
//        return defer.promise;
//
//      },
    
      loadOdataSets: function(set)
      {
        var defer = Q.defer();
        var serializePriority = _.bind(model.persistence.Serializer.priority.fromSAP, this);
        var serializeStatus = _.bind(model.persistence.Serializer.status.fromSAP, this);
        var serializeStatusInt = _.bind(model.persistence.Serializer.statusInt.fromSAP, this);
        var serializeCategory = _.bind(model.persistence.Serializer.category.fromSAP, this);
        var serializePartner = _.bind(model.persistence.Serializer.partner.fromSAP, this);
        var serializePartnerRole = _.bind(model.persistence.Serializer.partnerRole.fromSAP, this);
        var fSuccess = function(result)
        {
        var i = 0;
        var obj = {results:[]};
     
        switch(set){
            case "SSM_Priority":
                if(result.results && result.results.length>0){
                    for(i=0; i<result.results.length; i++){
                        var data = serializePriority(result.results[i]);
                        obj.results.push(data);
                    } 
                }
                break;
                
            case "SSM_Status":
                if(result.results && result.results.length>0){
                    for(i=0; i<result.results.length; i++){
                        var data = serializeStatus(result.results[i]);
                        obj.results.push(data);
                    } 
                }
                break;
                
            case "SSM_Status_Int":
                if(result.results && result.results.length>0){
                    for(i=0; i<result.results.length; i++){
                        var data = serializeStatusInt(result.results[i]);
                        obj.results.push(data);
                    } 
                }
                break;
                
            case "SSM_Category":
                if(result.results && result.results.length>0){
                    for(i=0; i<result.results.length; i++){
                        var data = serializeCategory(result.results[i]);
                        obj.results.push(data);
                    } 
                }
                break;
                
            case "SSM_PartnerList":
                if(result.results && result.results.length>0){
                    for(i=0; i<result.results.length; i++){
                        var data = serializePartner(result.results[i]);
                        obj.results.push(data);
                    } 
                }
                break;
                
            case "SSM_PartnerFCTList":
                if(result.results && result.results.length>0){
                    for(i=0; i<result.results.length; i++){
                        var data = serializePartnerRole(result.results[i]);
                        obj.results.push(data);
                    } 
                }
                break;
                
        }
          console.log("odata for "+set+" items loaded");
          var model  = new sap.ui.model.json.JSONModel(obj);
          defer.resolve(model);
        };
        fSuccess = _.bind(fSuccess, this);

        var fError = function(err)
        {
          console.log("Error Loading Items for " +set);
          defer.reject(err);
        }
        fError = _.bind(fError, this);

        model.odata.chiamateOdata.loadOdataSets(set,fSuccess, fError);
        return defer.promise;

      },
    
//      getOdataSelect: function( property, params)
//      {
//        var defer = Q.defer();
//        var fSuccess = function(result)
//        {
//          console.log(property+" loaded");
//          var model  = new sap.ui.model.json.JSONModel(result);
//          defer.resolve(model);
//        };
//        fSuccess = _.bind(fSuccess, this);
//
//        var fError = function(err)
//        {
//          sap.m.MessageToast.show("Error Loading Select "+ property);
//          defer.reject(err);
//        }
//        fError = _.bind(fError, this);
//
//        model.odata.chiamateOdata.getSelect(property, params, fSuccess, fError);
//        return defer.promise;
//
//      }
    };
