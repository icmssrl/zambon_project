
jQuery.sap.declare("model.collections.Tickets");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Ticket");
jQuery.sap.require("model.odata.chiamateOdata");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("sap.m.MessageToast");


model.collections.Tickets = ( function ()
{
  var tickets = [];
  //var defer = Q.defer();

  return {

    getById : function(code)
    {
        utils.Busy.show();
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
//        var ticket = _.find(result, _.bind(function(item)
//        {
//          return item.getId() === parseInt(code);
//        }, this));
        utils.Busy.hide();
        var data = model.persistence.Serializer.ticket.fromSAP(result);
        var ticket = new model.Ticket(data);
          
        if(ticket)
        {
          deferId.resolve(ticket); //
        }
        else
        {
          console.log("Tickets.js -- Ticket not found!");
        }
        
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
          utils.Busy.hide();
         console.log("Tickets.js -- Ticket not found!");
        deferId.reject(err);
      };
      
      fError = _.bind(fError, this);
        
      model.odata.chiamateOdata.getTicketDetail(code,fSuccess, fError);

//      this.loadTickets()
//      .then(fSuccess, fError);

      return deferId.promise;
    },


    loadTickets : function(user)
    {
        
        utils.Busy.show();
        var defer = Q.defer();
        var unameObj = {};
        unameObj.Uname = user.username;
//        if(unameObj.Uname === "rotapao1"){
//            unameObj.Bpartner = "211";
//        }else{
//            unameObj.Bpartner = "751";
//            unameObj.Bpartner = "713";
//        }
        
//        if(defer && defer.promise.isFulfilled())
//        {
//          defer.resolve(tickets);
//        }
//        else
//        {
          var fSuccess = function(result)
          {
              
            utils.Busy.hide();
            if(result)
            {
              tickets=[];
              var add = _.bind(this.addTicket, this);
              if(result.results && result.results.length>0){
                for(var i = 0; i <result.results.length; i++)
                  {
                    var data = model.persistence.Serializer.ticket.fromSAP(result.results[i]);
                    add(new model.Ticket(data));
                  }    
              }else{
                
                    var data = model.persistence.Serializer.ticket.fromSAP(result);
                    add(new model.Ticket(data));
                 
              } 
              

            }
            defer.resolve(tickets);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            utils.Busy.hide();
            tickets  = [];
            console.log("Error loading Tickets!");
            defer.reject(err);
          };
          fError = _.bind(fError, this);
            
            model.odata.chiamateOdata.getTicketList(unameObj,fSuccess, fError);

//          $.getJSON("custom/model/mock/data/tickets.json")
//            .success(fSuccess)
//            .fail(fError);
//        }
        // defer = Q.defer();



        return defer.promise;
    },
  
      
    loadFilteredTickets : function(filters)
    {
        
        utils.Busy.show();
        var defer = Q.defer();
//        var unameObj = {};
//        unameObj.Uname = user.username;
//        if(defer && defer.promise.isFulfilled())
//        {
//          defer.resolve(tickets);
//        }
//        else
//        {
          var fSuccess = function(result)
          {
              
            utils.Busy.hide();
            if(result)
            {
              tickets=[];
              var add = _.bind(this.addTicket, this);
              if(result.results && result.results.length>0){
                for(var i = 0; i <result.results.length; i++)
                  {
                    var data = model.persistence.Serializer.ticket.fromSAP(result.results[i]);
                    add(new model.Ticket(data));
                  }    
              }else{
                  
                    sap.m.MessageToast.show("0 results for the specified Filters!");
                    //var data = model.persistence.Serializer.ticket.fromSAP(result);
                    //add(new model.Ticket(data));
                 
              } 
              

            }
            defer.resolve(tickets);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            utils.Busy.hide();
            tickets  = [];
            console.log("Error loading Tickets with filters applied!");
            defer.reject(err);
          };
          fError = _.bind(fError, this);
            
            model.odata.chiamateOdata.getFilteredTicketList(filters,fSuccess, fError);

//          $.getJSON("custom/model/mock/data/tickets.json")
//            .success(fSuccess)
//            .fail(fError);
//        }
        // defer = Q.defer();



        return defer.promise;
    },
      
    addTicket : function(ticket)
    {
      tickets.push(ticket);
    },
    //--------------------------------------------------------------------


    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"tickets":[]};
      for(var i = 0; i< tickets.length; i++)
      {
        data.tickets.push(tickets[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
