
jQuery.sap.declare("model.collections.Partners");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.Partner");


model.collections.Partners = ( function ()
{
  var partners = [];
  //var defer = Q.defer();

  return {

    getById : function(code)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var partner = _.find(result, _.bind(function(item)
        {
          return item.getId() === code;
        }, this));
        if(partner)
        {
          deferId.resolve(partner); //
        }
        else
        {
          console.log("Partners.js -- Partner not found!");
        }
        
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      };
      
      fError = _.bind(fError, this);

      this.loadPartners()
      .then(fSuccess, fError);

      return deferId.promise;
    },
      
    loadPartners : function()
    {
        
        utils.Busy.show();
        var defer = Q.defer();
   
          var fSuccess = function(result)
          {
              
            utils.Busy.hide();
            if(result)
            {
              partners=[];
              var add = _.bind(this.addPartner, this);
              if(result.results && result.results.length>0){
                for(var i = 0; i <result.results.length; i++)
                  {
                    var data = model.persistence.Serializer.partner.fromSAP(result.results[i]);
                    add(new model.Partner(data));
                  }    
              }else{
                    partners=[];
                    //var data = model.persistence.Serializer.ticket.fromSAP(result);
                    //add(new model.Ticket(data));
                 
              } 
              

            }
            defer.resolve(partners);
          };
          fSuccess = _.bind(fSuccess, this);

          var fError = function(err)
          {
            utils.Busy.hide();
            partners  = [];
            console.log("Error loading Partners!");
            defer.reject(err);
          };
          fError = _.bind(fError, this);
            
            model.odata.chiamateOdata.getPartnersList(fSuccess, fError);



        return defer.promise;
    },
      
    addPartner : function(partner)
    {
      partners.push(partner);
    },
    //--------------------------------------------------------------------


    getModel : function()
    {
      var model = new sap.ui.model.json.JSONModel();
      var data = {"partners":[]};
      for(var i = 0; i< partners.length; i++)
      {
        data.partners.push(partners[i].getModel().getData());
      }
      model.setData(data);
      return model;
    }

  };
})();
