
jQuery.sap.declare("model.collections.NextStates");


model.collections.NextStates = 
{
 

    getNextStates : function(statusCode)
    {
      var deferId= Q.defer();
      var fSuccess  = function(result)
      {
        var statusObj = _.find(result, _.bind(function(item)
        {
          return item.status === statusCode;
        }, this));

        
        if(statusObj)
        {
          deferId.resolve(statusObj.nextStates); //
        }
        else
        {
          console.log("NextStates.js -- Next Status Object not found! I will give back the same status");
       
        }
        
      };
      fSuccess = _.bind(fSuccess, this);

      var fError  = function(err)
      {
        deferId.reject(err);
      };
      
      fError = _.bind(fError, this);

      $.getJSON("custom/model/mock/data/states.json")
      .success(fSuccess)
      .fail(fError);

      return deferId.promise;
    }
      
      

  }

