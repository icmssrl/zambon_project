jQuery.sap.declare("model.filters.Filter");
jQuery.sap.require("utils.ObjectUtils");
jQuery.sap.require("model.i18n");

model.filters.Filter = {


//  checkFilterExistence: function(collection)
//  {
//    return (this.filterData && this.filterData[collection] && this.filterData[collection].items && this.filterData[collection].items.length > 0);
//
//  },


  deleteFilter:function(collection)
  {
    this.filterData[collection] = undefined;
  },
  resetFilter:function(collection)
  {
    var selectedItems = this.getSelectedItems(collection);
    for(var i = 0; i< selectedItems.length; i++)
    {
      selectedItems[i].isSelected=false;
    }
    return;
  },
  getFilterData:function(collection)
  {
//    if(!this.checkFilterExistence(collection))
//      return null;
    return this.filterData[collection];

  },
  getSelectedItems: function(collection)
  {
    var filterItems = [];
//    if(!this.checkFilterExistence(collection))
//    {
//      return null;
//    }
    var items = this.filterData[collection].items;
		for(var i = 0 ; i< items.length; i++)
		{
			for(var j=0; j< items[i].values.length; j++)
			{
				if(items[i].values[j].isSelected)
				{
					filterItems.push(items[i].values[j]);
				}
			}
		}
		return filterItems;
  },

  getModel:function(list, collection)
  {
    if(!list || list.length==0)
      return;


//    if(!this.checkFilterExistence(collection))
//    {
      this.filterData={};
      this.filterData[collection] = {"items":[]};
      // this.props[collection] = [];
//    }

    _.forEach(list,  _.bind(
      function (item)
      {
        var itemProps = utils.ObjectUtils.getKeys(item);

        if(!itemProps || itemProps.length == 0)
          return;

        _.forEach(itemProps, _.bind(
          function(collection, prop)
          {
            var filterPropertyValue = utils.ObjectUtils.getValues(item, prop);
            this.addFilterItem(prop, filterPropertyValue, collection)

          },this, collection));


      }, this));


    console.log(this.filterData[collection]);
    return new sap.ui.model.json.JSONModel(this.filterData[collection]);
  },

  addFilterItem: function(prop, value, collection)
  {
//    if(!this.checkFilterExistence(collection))
//    {
//      this.filterData[collection].items=[];
//    }
    var parentItem = _.find(this.filterData[collection].items, {'propertyName' : prop});
    if(!parentItem)
    {
      parentItem = {"propertyName": prop, "title": model.i18n._getLocaleText(this._getTitle(prop)),"values":[]};
      this.filterData[collection].items.push(parentItem);
    }
    if(!_.find(parentItem.values, {"value": value}))
    {
      parentItem.values.push({"value":value, "property": prop, "isSelected": false});
    }
    return;

  },
  _getTitle:function(prop)
  {
    if(prop.indexOf("/") !== -1){
        var barIdx = prop.lastIndexOf("/");
    }
    if(barIdx && barIdx < 0)
    {
      return prop;
    }
    var text = prop.substring(barIdx+1, prop.length);
    if(prop.indexOf("/") !== barIdx)
    {
      text = prop.substring(0, prop.indexOf("/")) + text;
    }


    return text;
  }



}
