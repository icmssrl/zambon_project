//jQuery.sap.declare("model.PopulateFilter");
//jQuery.sap.require("icms.Settings");
//jQuery.sap.require("icms.Component");
//
//model.PopulateFilter =
//{
//   
//    
//    ticketIdList:[],
//    reportedByList:[], 
//    priorityList:[],
//    statusList:[],
//    descriptionList:[],
//    recipientList:[], 
//    ticketTypeList:[], 
//    supportTeamList:[], 
//    deliveryDateList:[],
//    deliveryDateCounterStopList:[],
//    _items:[],
//    _deferTicketId: Q.defer(),
//    _deferReportedBy: Q.defer(),
//    _deferPriority: Q.defer(),
//    _deferStatus: Q.defer(),
//    _deferDescription: Q.defer(),
//    _deferRecipient: Q.defer(),
//    _deferTicketType: Q.defer(),
//    _deferSupportTeam: Q.defer(),
//    _deferDeliveryDateList: Q.defer(),
//    _deferDeliveryDateCounterStop: Q.defer(),
//    
//     _serviceUrl: icms.Component.getMetadata().getConfig().settings.serverUrl,
//
//
//      getOdataModel: function () {
//        if (!this._odataModel) {
//          this._odataModel = new sap.ui.model.odata.ODataModel(this._serviceUrl, true);
//        }
//        return this._odataModel;
//      },
//
//    getInstance:function()
//    {
//        return this;
//    },
//    
//    loadTicketIdList:function(user, forceReload)
//    {
//        if(forceReload)
//        {
//            this._deferTicketId = Q.defer();
//        }
//        //  this._deferCustomers = Q.defer();
//        if(this._deferTicketId.promise.isFulfilled())
//        {
//            var _parseToItems = _.bind(this._parseToItems, this);
//            this._items=_parseToItems(this.ticketIdList);
//            this._deferTicketId.resolve(this._items);
//        }
//        else
//        {
//            var fSuccess = function(result)
//            {   
//                result = JSON.parse(result);
//                this.ticketIdList=result.results;
//                this._items = this._parseToItems(this.ticketIdList);
//                this._deferTicketId.resolve(this._items);
//            }
//            var fError = function(err)
//            {
//                console.log("Error loading Tickets");
//                this._deferTicketId.reject(err)
//            }
//            
//            fSuccess = _.bind(fSuccess, this);
//            fError = _.bind(fError, this);
//            
//          
//            this.getOdataModel().read("SSM_User_DataSetSet?$filter=Uname eq '" + user.username + "'", {
//              success: fSuccess,
//              error: fError,
//              async: true
//            });
//        }
//        
//         return this._deferTicketId.promise;
//    },
//    
//    loadDeliveryDates:function(user, forceReload)
//    {
//        if(forceReload)
//        {
//            this._deferDeliveryDateList = Q.defer();
//        }
//        //  this._deferCustomers = Q.defer();
//        if(this._deferDeliveryDateList.promise.isFulfilled())
//        {
//            var _parseToItems = _.bind(this._parseToItems, this);
//            this._items=_parseToItems(this.deliveryDateList);
//            this._deferDeliveryDateList.resolve(this._items);
//        }
//        else
//        {
//            var fSuccess = function(result)
//            {   
//                result = JSON.parse(result);
//                this.deliveryDateList=result.results;
//                this._items = this._parseToItems(this.deliveryDateList);
//                this._deferDeliveryDateList.resolve(this._items);
//            }
//            var fError = function(err)
//            {
//                console.log("Error loading Dates");
//                this._deferDeliveryDateList.reject(err)
//            }
//            
//            fSuccess = _.bind(fSuccess, this);
//            fError = _.bind(fError, this);
//            
//          
//            this.getOdataModel().read("SSM_User_DataSetSet?$filter=Uname eq '" + user.username + "'", {
//              success: fSuccess,
//              error: fError,
//              async: true
//            });
//        }
//        
//         return this._deferDeliveryDateList.promise;
//    },
//   
//    
//    _parseToItems:function(data)
//    {
//        
//        return _.map(data, function(element)
//        {
//            var obj={"name":"", "id":""};
//            for(var prop in element)
//            {
//                if(prop.indexOf("Name")>-1)
//                {
//                    obj.name=element[prop];
//                }
//                else if(prop.indexOf("ID")>-1)
//                {
//                    obj.id = element[prop];
//                }
//                
//            }
//            return obj;
//        })
//    },
//    
//   
//}