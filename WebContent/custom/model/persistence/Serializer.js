jQuery.sap.declare("model.persistence.Serializer");
jQuery.sap.require("model.Partner");

model.persistence.Serializer = {

ticket:{


    fromSAP: function(sapData)
    {
      var o = {};
        var arraynote = [];        
        var note = {};
        var arrayDocs = [];
        var document = {};
        var partners = [];
        var partner = {};
        var content = {};
        o.ticketId = sapData.ObjectId ? sapData.ObjectId : "";
        o.description = sapData.Description ? sapData.Description : "";
//        o.priority = sapData.PriorityTxt.substring(3) ? sapData.PriorityTxt.substring(3) : "";
        o.priority = sapData.PriorityTxt ? sapData.PriorityTxt : "";
        o.priorityCode = sapData.Priority ? sapData.Priority : "";
        o.statusCode = sapData.Estat ? sapData.Estat : "";
        o.status = sapData.EstatTxt ? sapData.EstatTxt : "";
        o.completion = sapData.Completion ? sapData.Completion : "";
        o.messageProcessor = sapData.MessprocTxt ? sapData.MessprocTxt : "";
        o.messageProcessorCode = sapData.Messproc ? sapData.Messproc : "";
        o.lastModifiedDate = sapData.ChangeDate ? sapData.ChangeDate : "";
//        o.lastModifiedDate = sapData.ChangeTz ? sapData.ChangeTz : "";
        o.creationDate = sapData.PostingDate ? sapData.PostingDate : "";
//        o.creationDate = sapData.PostingTz ? sapData.PostingTz : "";
        o.creationDateFromList = sapData.PostingDate ? sapData.PostingDate : "";
        o.createdBy = sapData.CreatedByTxt ? sapData.CreatedByTxt : "";
        o.createdByCode = sapData.CreatedBy ? sapData.CreatedBy : "";
        o.reportedBy = sapData.ReportedByTxt ? sapData.ReportedByTxt : "";
        o.reportedByCode = sapData.ReportedBy ? sapData.ReportedBy : "";
        o.supportTeam = sapData.SupportTeamTxt ? sapData.SupportTeamTxt : "";
        o.supportTeamCode = sapData.SupportTeam ? sapData.SupportTeam : "";
        o.recipient = sapData.RecipientTxt ? sapData.RecipientTxt : "";
        o.recipientCode = sapData.Recipient ? sapData.Recipient : "";
        o.ticketCategory = sapData.CategoryTxt ? sapData.CategoryTxt : "";
        o.ticketCategoryCode = sapData.Category ? sapData.Category : "";
        o.deliveryDate = sapData.ZzdelivDate ? sapData.ZzdelivDate : "";


        if(sapData.ZzdelivTime && sapData.ZzdelivTime.ms){
            o.deliveryTime = sapData.ZzdelivTime.ms;
        }else{
            o.deliveryTime = "";
        }
        o.deliveryDateCounterStop = sapData.ZzdelivDateCs ? sapData.ZzdelivDateCs : "";
        if(sapData.ZzdelivTimeCs && sapData.ZzdelivTimeCs.ms){
            o.deliveryTimeCounterStop = sapData.ZzdelivTimeCs.ms;
        }else{
            o.deliveryTimeCounterStop = "";
        }

        o.deliveryDateManual = sapData.ZzorderadmH0201 ? sapData.ZzorderadmH0201 : "";
        o.deliveryDateEval = sapData.ZzorderadmH0202 ? sapData.ZzorderadmH0202 : "";
        o.ticketValue = sapData.ZzticketValue ? sapData.ZzticketValue : "";
        o.currency = sapData.ZzticketVCuky ? sapData.ZzticketVCuky : "";
        o.orderNumber = sapData.ZzorderNum ? sapData.ZzorderNum : "";
        o.em30Percent = sapData.Zzvalue30 ? sapData.Zzvalue30 : "";
        o.em30Currency = sapData.Zzvalue30Cu ? sapData.Zzvalue30Cu : "";
        o.em70Percent = sapData.Zzvalue70 ? sapData.Zzvalue70 : "";
        o.em70Currency = sapData.Zzvalue70Cu ? sapData.Zzvalue70Cu : "";

        o.nextStates = sapData.nextStates ? sapData.nextStates : [];

//        if(sapData.DetailToNotes)
//        if(sapData.DetailToNotes.results && sapData.DetailToNotes.results.length > 0){
//            for(var i = 0; i< sapData.DetailToNotes.results.length; i++)
//            {
//                note = model.persistence.Serializer.note.fromSAP(sapData.DetailToNotes.results[i]);
//                arraynote.push(note);
//            }
//        }
        if(sapData.SSM_Ticket_DocumentSet)
        if(sapData.SSM_Ticket_DocumentSet.results && sapData.SSM_Ticket_DocumentSet.results.length > 0){
            for(var i = 0; i< sapData.SSM_Ticket_DocumentSet.results.length; i++)
            {
                document = model.persistence.Serializer.document.fromSAP(sapData.SSM_Ticket_DocumentSet.results[i]);
                arrayDocs.push(document);
            }
        }
        if(sapData.DetailToPartners)
        //var serialize = _.bind(this.partner.fromSAP, this);
        if(sapData.DetailToPartners.results && sapData.DetailToPartners.results.length > 0){
            for(var i = 0; i< sapData.DetailToPartners.results.length; i++)
            {


                partner = model.persistence.Serializer.partner.fromSAP(new model.Partner(sapData.DetailToPartners.results[i]));
                partners.push(partner);
            }
        }

        if(sapData.DetailToTexts)
        if(sapData.DetailToTexts.results && sapData.DetailToTexts.results.length > 0){
            var contentString = "";
            for(var i = 0; i< sapData.DetailToTexts.results.length; i++)
            {
                content = model.persistence.Serializer.content.fromSAP(sapData.DetailToTexts.results[i]);
                contentString += content.newLine + "\n";
            }

        }
        o.ticketContent = contentString ? contentString : "";
//        o.notes = arraynote ? arraynote : [];
        o.associatedPartners = partners ? partners : [];
        o.docs = arrayDocs ? arrayDocs : [];

      return o;
    },

    toSAP: function(obj)
    {
        var toSAPData = {};
        var noteSerializedBack = {};
        var partnerSerializedBack = {};
        
        toSAPData.IObjectId= obj.ticketId ? obj.ticketId : "";
        toSAPData.ObjectId= obj.ticketId ? obj.ticketId : "";
//        toSAPData.Category = obj.ticketCategoryCode ? obj.ticketCategoryCode : "";
        toSAPData.Estat = obj.statusCode ? obj.statusCode : "";
        toSAPData.TextString = obj.newContentString ? obj.newContentString : "";
//        toSAPData.ZzdelivDate = null;
//        toSAPData.ZzdelivTime = "";
//        toSAPData.ZzdelivDateCs = null;
//        toSAPData.ZzdelivTimeCs = "";
//        toSAPData.ZzorderadmH0201 = null;
//        toSAPData.ZzorderadmH0202 = null;
//        toSAPData.Messproc= obj.messageProcessorCode ? obj.messageProcessorCode : "";
        toSAPData.Priority= obj.priorityCode ? obj.priorityCode : "";
//        toSAPData.Recipient= obj.recipientCode ? obj.recipientCode : "";
//        toSAPData.ReportedBy= obj.reportedByCode ? obj.reportedByCode : "";
//        toSAPData.SupportTeam= obj.supportTeamCode ? obj.supportTeamCode : "";
        
//        var ident = 1;
//        if(obj.notes && obj.notes.length > 0){
////            var detailNotes = {results:[]};
//            var detailNotes = [];
//            for(var e = 0; e < obj.notes.length; e++){
//                noteSerializedBack = model.persistence.Serializer.note.toSAP(obj.notes[e]);
//          
////                noteSerializedBack.Ident = ident.toString();
////                ident =ident+1;
//
//                detailNotes.push(noteSerializedBack);
//            }
//        toSAPData.DetailToNotes = detailNotes;
//        }
        
        if(obj.associatedPartners && obj.associatedPartners.length > 0){
//            var detailPartners = {results:[]};
            var detailPartners = [];
            for(var c = 0; c < obj.associatedPartners.length; c++){
                partnerSerializedBack = model.persistence.Serializer.partner.toSAP(obj.associatedPartners[c]);
                detailPartners.push(partnerSerializedBack);
            }
        toSAPData.DetailToPartners = detailPartners;

        }
        console.log(toSAPData);
        return toSAPData;

    }
    },

    partner:{
        fromSAP: function(sapData)
        {
          var p = {};


            p.address = sapData.Address ? sapData.Address : "";
            if(sapData.Mainpartner){
                if(sapData.Mainpartner === "X"){
                    p.mainPartner=true;
                }else{
                    p.mainPartner=false;
                }
            }else{
                p.mainPartner=false;
            }

            p.ticketId = sapData.ObjectId ? sapData.ObjectId : "";
            p.partnerFunction = sapData.Description ? sapData.Description : "";
            p.partnerFunctionCode = sapData.PartnerFct ? sapData.PartnerFct : "";
            p.partnerNr = sapData.PartnerNo ? sapData.PartnerNo : "";

            p.partnerId = sapData.Partner ? sapData.Partner : "";
            p.partnerName = sapData.NameFirst ? sapData.NameFirst : "";
            p.partnerLastName = sapData.NameLast ? sapData.NameLast : "";
            p.partnerOrgName = sapData.NameOrg1 ? sapData.NameOrg1 : "";

          return p;
        },

        toSAP: function(obj)
        {
            var toSAPData = {};
            if(obj.mainPartner)
            if(obj.mainPartner === true){
                toSAPData.Mainpartner = "X";
            }else{
                toSAPData.Mainpartner = "";
            }
            toSAPData.PartnerFct= obj.partnerFunctionCode ? obj.partnerFunctionCode : "";
            toSAPData.ObjectId= obj.ticketId ? obj.ticketId : "";
            if(obj.partnerNr.trim().length>0 && obj.partnerId.trim().length===0){
                toSAPData.PartnerNo = obj.partnerNr;
            }else{
                if(obj.partnerNr.trim().length===0 && obj.partnerId.trim().length>0){
                    toSAPData.PartnerNo = obj.partnerId;
                }else{
                    toSAPData.PartnerNo = "";
                }
            }

            return toSAPData;
        }
    },

    note:{
        fromSAP: function(sapData)
        {
          var n = {};
            
            n.noteDescription = sapData.Description ? sapData.Description : "";
            n.ticketId = sapData.ObjectId ? sapData.ObjectId : "";
            n.noteType = sapData.TypeNote ? sapData.TypeNote : "";
            n.noteId = sapData.Ident ? sapData.Ident : "";

          return n;
        },

        toSAP: function(obj)
        {
            
            var toSAPData = {};
            toSAPData.ObjectId= obj.ticketId ? obj.ticketId : "";
            toSAPData.Description= obj.noteDescription ? obj.noteDescription : "";
            toSAPData.Ident = obj.noteId ? obj.noteId : "";
                
                
//            toSAPData.TypeNote= obj.noteType ? obj.noteType : "";
            return toSAPData;
        }
    },
    
    category:{
        fromSAP: function(sapData)
        {
          var c = {};

            c.Client = sapData.Client ? sapData.Client : "";
            c.Langu = sapData.Langu ? sapData.Langu : "";
            c.categoryName = sapData.Description ? sapData.Description : "";
            c.categoryCode = sapData.Category ? sapData.Category : "";

          return n;
        },

        toSAP: function(obj)
        {
            var toSAPData = {};
                toSAPData.Category= obj.categoryCode ? obj.categoryCode : "";

            return toSAPData;
        }
    },

    priority:{
        fromSAP: function(sapData)
        {
          var p = {};

            p.clientId = sapData.Client ? sapData.Client : "";
            p.Langu = sapData.Langu ? sapData.Langu : "";
            p.priorityCode = sapData.Priority ? sapData.Priority : "";
            p.priorityTxt = sapData.TxtLong ? sapData.TxtLong : "";

          return p;
        },

        toSAP: function(obj)
        {
           var toSAPData = {};

          return toSAPData;
        }
    },

    partnerRole:{
        fromSAP: function(sapData)
        {
          var p = {};

            p.partnerRole = sapData.Description ? sapData.Description : "";
            p.shortRoleTxt = sapData.ShortDesc ? sapData.ShortDesc : "";
            p.roleCode = sapData.PartnerFct ? sapData.PartnerFct : "";


          return p;
        },

        toSAP: function(obj)
        {
           var toSAPData = {};

          return toSAPData;
        }
    },


    content:{
        fromSAP: function(sapData)
        {
          var c = {};

            c.lineIndex = sapData.Counter ? sapData.Counter : "";
            c.ticketId = sapData.ObjectId ? sapData.ObjectId : "";
            c.newLine = sapData.Tdline ? sapData.Tdline : "";
            c.format = sapData.Tdformat ? sapData.Tdformat : "";

          return c;
        },

        toSAP: function(obj)
        {
           var toSAPData = {};

          return toSAPData;
        }
    },

    status:{


        fromSAP: function(sapData)
        {
          var s = {};

            s.statusCode = sapData.Estat ? sapData.Estat : "";
            s.statusLabel = sapData.Txt04 ? sapData.Txt04 : "";
            s.statusText = sapData.Txt30 ? sapData.Txt30 : "";
            s.Spras = sapData.Spras ? sapData.Spras : "";
            s.Stsma = sapData.Stsma ? sapData.Stsma : "";
            s.Ltext = sapData.Ltext ? sapData.Ltext : "";

          return s;
        },

        toSAP: function(obj)
        {
           var toSAPData = {};

          return toSAPData;
        }
    },
    
    statusInt:{


        fromSAP: function(sapData)
        {
          var s = {};

            s.statusCode = sapData.Status ? sapData.Status : "";
            s.statusLabel = sapData.Txt04 ? sapData.Txt04 : "";
            s.statusText = sapData.Txt30 ? sapData.Txt30 : "";

          return s;
        },

        toSAP: function(obj)
        {
           var toSAPData = {};

          return toSAPData;
        }
    },
    
    document: {
       fromSAP: function(sapData)
        {
          var d = {};
            
            d.ticketId = sapData.ObjectId ? sapData.ObjectId : "";
            d.fileName = sapData.FileName ? sapData.FileName : "";
            d.mimetype = sapData.Mimetype ? sapData.Mimetype : "";
            d.description = sapData.Description ? sapData.Description : "";
            d.Objtypelo = sapData.Objtypelo ? sapData.Objtypelo : "";
            d.Classlo = sapData.Classlo ? sapData.Classlo : "";
            d.Objidlo = sapData.Objidlo ? sapData.Objidlo : "";
            d.Objtypeph = sapData.Objtypeph ? sapData.Objtypeph : "";
            d.Classph = sapData.Classph ? sapData.Classph : "";
            d.Objidph = sapData.Objidph ? sapData.Objidph : "";

          return d;
        },
        toSAP: function(obj)
        {
           var toSAPData = {};

          return toSAPData;
        }

    },


    file: {
       fromSAP: function(sapData)
        {
          var f = {};

            f.fileName = sapData.fileName ? sapData.fileName : "";
            f.fileType = sapData.fileType ? sapData.fileType : "";
            f.filedescription = sapData.filedescription ? sapData.filedescription : "";

          return f;
        },
        toSAP: function(obj)
        {
           var toSAPData = {};

          return toSAPData;
        }

    },

    userInfo: {

    fromSAP: function (sapData) {
      var u = {}

      u.username = sapData.Uname ? sapData.Uname : "";
      u.firstName = sapData.Firstname ? sapData.Firstname : "";
      u.lastName = sapData.Lastname ? sapData.Lastname : "";
      u.userAlias = sapData.Ualias ? sapData.Ualias : "";
      u.title = sapData.TitleP ? sapData.TitleP : "";
      u.userType = sapData.Class ? sapData.Class : "";
      u.inputUsername = sapData.IUname ? sapData.IUname : "";
      u.inputPassword = sapData.IUncode ? sapData.IUncode : "";
      u.inputAlias = sapData.IAlias ? sapData.IAlias : "";
      u.personalNumber = sapData.PersNo ? sapData.PersNo : "";
      u.addressNumber = sapData.AddrNo ? sapData.AddrNo : "";


      return u;
    }
  }

};
