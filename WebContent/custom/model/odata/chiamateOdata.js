jQuery.sap.declare("model.odata.chiamateOdata");
jQuery.sap.require("icms.Settings");
jQuery.sap.require("icms.Component");
jQuery.sap.require("utils.Busy");

model.odata.chiamateOdata = {
  _serviceUrl: icms.Component.getMetadata().getConfig().settings.serverUrl,


  getOdataModel: function () {
    if (!this._odataModel) {
      this._odataModel = new sap.ui.model.odata.ODataModel(this._serviceUrl, true);
    }
    return this._odataModel;
  },

  getUserById: function (id, success, error) {
    this.getOdataModel().read("SSM_User_DataSetSet?$filter=IUname eq '" + id + "'", {
      success: success,
      error: error,
      async: true
    });
  },


  login: function (user, pwd, success, error) {
    this.getOdataModel().read("SSM_User_DataSetSet?$filter=IUname  eq '" + user + "' and I_UNCODE eq '" + pwd + "'", {
      success: success,
      error: error,
      async: true
    });
  },

  //-----------------------------------------------------------------------------------


  getTicketList: function (req, success, error) {
    //req is an object containing parameters passed to Odata then It's built the url
    var url = "SSM_Ticket_listSet";
    var isFirst = true;
    for (var prop in req) {
      if (_.isEmpty(req[prop]))
        continue;

      if (isFirst) {
        url = url.concat("?$filter=");
        isFirst = false;
      } else {
        url = url.concat(" and ");
      }


      var propString = prop + " eq '" + req[prop] + "'";
      url = url.concat(propString);
    }
    this.getOdataModel().read(url, {
      success: success,
      error: error,
      async: true
    });
  },

    getFilteredTicketList: function (filters, success, error) {

    var url = "SSM_Ticket_listSet";

    this.getOdataModel().read(url, {
      success: success,
      error: error,
      filters: filters,
      async: true
    });
  },


  getTicketDetail: function(ticketId, success, error)
  {
    //var url = "SSM_Ticket_detail?$filter=ObjectId eq '"+ticketId+"'";
    var url = "SSM_Ticket_DetailSet('"+ticketId+"')?$expand=DetailToNotes,DetailToPartners,DetailToTexts,SSM_Ticket_DocumentSet";


    this.getOdataModel().read(url, {success:success, error:error, async:true});

  },

  getPartnersList: function(success, error)
  {
    var url = "SSM_PartnerListSet";
    this.getOdataModel().read(url, {success:success, error:error, async:true});

  },

  getNextStates: function(idTicket, success, error)
  {
    var url = "SSM_StatusSet?$filter=ObjectId eq '"+idTicket+"'";
    this.getOdataModel().read(url, {success:success, error:error, async:true});

  },
    
  getFileFromServer: function(data, success, error){
    var url = "Ticket_MediaSet";
    url += "(Objtypelo='" + data.Objtypelo + "',";
    url += "Classlo='" + data.Classlo + "',";
    url += "Objidlo='" + data.Objidlo + "',";
    url += "Objtypeph='" + data.Objtypeph + "',";
    url += "Classph='" + data.Classph + "',";
    url += "Objidph='" + data.Objidph + "',";
    url += "FileName='"+ data.fileName + "',";
    url += "Mimetype='')/$value";
//       + data.mimetype + 
//       + data.fileName + 
      var uri = encodeURI(url);
      utils.Busy.hide();
      sap.m.URLHelper.redirect(this._serviceUrl+uri, true);
      
//    this.getOdataModel().read(url, {
//      success: success,
//      error: error,
//      async: true
//    });
  },

//  createCustomer: function (data, success, error) {
//    url = "CustomerSet"
//    var entity = {};
//    for (var prop in data) {
//      if (!data[prop])
//        continue;
//      entity[prop] = data[prop];
//    }
//    entity.Kunnr = "";
//
//    this.getOdataModel().create(url, entity, {
//      success: success,
//      error: error
//    });
//  },

//  updateTicket: function (ticketId, ticket, success, error) {
//    url = "SSM_Ticket_DetailSet('"+ ticketId +"')";
////    var entity = {};
////    for (var prop in ticket) {
////      if (!ticket[prop])
////        continue;
////      entity[prop] = ticket[prop];
////    }
////    entity.ObjectId = "";
//
//    this.getOdataModel().update(url, ticket, {
//      success: success,
//      error: error
//    });
//  },

    
    updateTicket: function (ticketId, ticket, success, error) {
    url = "SSM_Ticket_DetailSet"; //('"+ticketId+"')
//    var entity = {};
//    for (var prop in ticket) {
//      if (!ticket[prop])
//        continue;
//      entity[prop] = ticket[prop];
//    }
//    entity.ObjectId = "";

    this.getOdataModel().create(url, ticket, {
      success: success,
      error: error
    });
  },
  



    loadOdataSets: function (set, success, error) {
    var url = set+"Set";
    this.getOdataModel().read(url, {
      success: success,
      error: error
    })
  },



//    getSelect: function (property, params, success, error) {
//    var url = "F4_" + property + "Set";
//    var filters = [];
//    for (var prop in params) {
//      filters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, params[prop]));
//    }
//    this.getOdataModel().read(url, {
//      filters: filters,
//      success: success,
//      error: error
//    })
//  },

    //////***************************************


//  updateCustomer: function (data, success, error) {
//    url = "CustomerSet"
//    url += "(Kunnr='" + data.Kunnr + "',";
//    url += "Bukrs='" + data.Bukrs + "',";
//    url += "Vkorg='" + data.Vkorg + "',";
//    url += "Vtweg='" + data.Vtweg + "',";
//    url += "Spart='" + data.Spart + "',";
//    url += "Cdage='" + data.Cdage + "')";
//
//
//    this.getOdataModel().update(url, data, {
//      success: success,
//      error: error
//    });
//
//  },
//
//
//
//  getProductDetail: function (req, success, error) {
//    var url = "MatnrPriceSet";
//    // var isFirst = true;
//    var filters = [];
//    var matFilters = [];
//
//    for (var prop in req) {
//      if (_.isEmpty(req[prop]))
//        continue;
//      if (prop !== "Matnr" && prop !== "Maktx") {
//        filters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, req[prop]));
//      } else {
//        if (prop === "Matnr")
//          matFilters.push(new sap.ui.model.Filter(prop, sap.ui.model.FilterOperator.EQ, req[prop]));
//
//      }
//
//    }
//    matFilters = new sap.ui.model.Filter({
//      filters: matFilters,
//      and: false
//    });
//    filters.push(matFilters);
//
//    this.getOdataModel().read(url, {
//      filters: filters,
//      success: success,
//      error: error,
//      async: true
//    });
//  },
//
//
//  createOrder: function (order, success, error) {
//    url = "SalesOrderHeaderSet";
//    var entity = {};
//    for (var prop in order) {
//      if (!order[prop])
//        continue;
//      entity[prop] = order[prop];
//    }
//    entity.Vbeln = "";
//
//    this.getOdataModel().create(url, entity, {
//      success: success,
//      error: error
//    });
//  },





};
