jQuery.sap.declare("model.Partner");

model.Partner = (function () {

  Partner = function (serializedData) {
    
    this.partnerId = undefined;
    this.partnerName = undefined;
    this.partnerLastName = undefined;
    this.partnerOrgName = undefined;
    this.address = undefined;
    this.mainPartner = undefined;
    this.ticketId = undefined;
    this.partnerFunction = undefined;
    this.partnerFunctionCode = undefined;
    this.partnerNr = undefined;
    
    

    this.getId = function () {
      return this.partnerId;
    };
      
    this.setPartnerFunctionCode = function (code) {
      this.partnerFunctionCode = code;
    };
      
   this.setPartnerFunction = function (functionName) {
      this.partnerFunction = functionName;
    };
      
    this.setMainPartner = function (value) {
      this.mainPartner = value;
    };
   

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };


    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };

    if (serializedData) {
      this.update(serializedData);
    }
    return this;
  };
  return Partner;


})();
