jQuery.sap.declare("model.User");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("model.odata.chiamateOdata");

//jQuery.sap.require("model.odata.chiamateOdata");


model.User = ( function (){

  User  = function(data)
  {
      this.username = "";
      this.password  ="";
      this.firstName = "";
      this.lastName = "";
      this.userAlias = "";
      this.title =  "";
      this.userType = "";
      this.inputAlias = "";
      this.personalNumber = "";
      this.addressNumber = "";

   this.setCredential = function(uname, pwd)
    {
      this.username = uname;
      this.password = pwd;
    };

   this.setCurrentUser = function(userInfo)
   {
     this.username = userInfo.username ;
     this.password = userInfo.password ;
     this.userType = userInfo.userType;

   };
   this.getCurrentUser = function()
   {
     return this;
   };
   this.getUsername = function()
   {
     return this.username;
   };
   this.setUserName = function(value)
   {
     this.username = value;
   };
   this.setPassword = function(value)
   {
     this.password  = value;
   };

   this.getNewCredential = function()
   {
     this.username = "";
     this.password = "";
     return this.getModel();
   };
      
    this.update=function(data)
    {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };

   this.getModel = function()
   {
     var model = new sap.ui.model.json.JSONModel();
     var user = this.getCurrentUser();
     model.setData(user);
     return model;
   };
      
    this.doLogin = function(u)
    {
        var loginDefer = Q.defer();
        utils.Busy.show();

        var fSuccess = function(result)
        {
          utils.Busy.hide();
          var user = model.persistence.Serializer.userInfo.fromSAP(result.results[0]);
          model.persistence.Storage.session.save("user", user);
          loginDefer.resolve(user);

        };
        fSuccess = _.bind(fSuccess, this);

        var fError  = function(err)
        {
          utils.Busy.hide();
          loginDefer.reject(err);
        };
        fError = _.bind(fError, this);

          model.odata.chiamateOdata.getUserById(u,fSuccess,fError);

        return loginDefer.promise;
      };

//   this.doLogin = function()
//   {
//
//       var defer = Q.defer();
//       utils.Busy.show();
//
//       var fSuccess = function(result)
//       {
//         utils.Busy.hide();
//
//           var user;
//           var that = this;
//           result.map(function(obj){     
//                            if (obj.username.toUpperCase() === that.username.toUpperCase()
//                               & obj.password === that.password) 
//                            user = obj;
//                        });
//         if(!user){
//           defer.reject("user not found");
//           sap.m.MessageToast.show("user not found");
//         }else {
//           this.setCurrentUser(user);
//           console.log("userInLogin");
//           console.log(user);
//
//           model.persistence.Storage.session.save("user", user);
//           defer.resolve(user);
//         }
//       };
//       var fError = function(err)
//       {
//         utils.Busy.hide();
//         defer.reject(err);
//       };
//       fSuccess = _.bind(fSuccess, this);
//       fError = _.bind(fError,this);
//
//       $.getJSON("custom/model/mock/data/users.json")
//         .success(fSuccess)
//         .fail(fError);
//
//
//
//       return defer.promise;
//     };
      

     this.sessionSave = function()
     {
       var user = {username : this.username, type: this.userType};
       model.persistence.Storage.session.save("userType", user );
     };
         
    if(data)
        this.update(data);

      return this;
      
  };
    
    return User;


  })();
