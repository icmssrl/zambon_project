jQuery.sap.declare("model.Tiles");
jQuery.sap.require("model.i18n");


model.Tiles = function () {



    TileCollection = [
    {
            "icon": "timesheet",
            "title": model.i18n._getLocaleText("TICKET_MANAGEMENT"),
            "url": "noDataSplitDetail",
            "type": "user"
     },
     {
            "icon": "timesheet",
            "title": model.i18n._getLocaleText("TICKET_MANAGEMENT"),
            "url": "noDataSplitDetail",
            "type": "admin"
     }
//        ,
//     {
//            "icon": "manager-insight",
//            "title": model.i18n._getLocaleText("TICKET_REPORT"),
//            "url": "ticketReport",
//            "type": "admin"
//     }

  ];

    //  societyTiles = [
    //    {
    //      "textColor":"lightgreen",
    //      "backgroundColor":"white",
    //      "icon": "./custom/img/loghi/beretta.gif",
    //      "title": "Beretta",
    //      "url": "launchpad",
    //      "society": "SI41"
    //    },
    //    {
    //      "textColor":"red",
    //      "backgroundColor":"white",
    //      "icon": "./custom/img/loghi/riello.gif",
    //      "title": "Riello Trade",
    //      "url": "launchpad",
    //      "society": "SI01"
    //    },
    //    {
    //        
    //      "textColor":"lightgreen",
    //      "backgroundColor":"white",
    //      "icon": "./custom/img/loghi/beretta.gif",
    //      "title": "Beretta",
    //      "url": "launchpad",
    //      "society":"CF"
    //    }
    //
    //  ];



    getTiles = function (type) {

        arrayTiles = _.where(TileCollection, {
            'type': type
        });
        return arrayTiles;

    };

    //  getSocietyTile = function(society)
    //  {
    //    return _.find(societyTiles, {society : society});
    //  };

    return {
        getMenu: getTiles

    };



}();
