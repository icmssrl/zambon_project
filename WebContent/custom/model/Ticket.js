jQuery.sap.declare("model.Ticket");

model.Ticket = (function () {

  Ticket = function (serializedData) {
    
    this.ticketId = undefined;
    this.description = undefined;
    this.ticketContent = undefined;
    this.priority = undefined;
    this.priorityCode = undefined;
    this.status = undefined;
    this.statusCode = undefined;
    this.completion = undefined;
    this.messageProcessor = undefined;
    this.messageProcessorCode = undefined;
    this.lastModifiedDate = undefined;
    this.creationDate = undefined;
    this.creationDateFromList = undefined;
    this.createdBy = undefined;
    this.createdByCode = undefined;
    this.reportedBy = undefined;
    this.reportedByCode = undefined;
    this.supportTeam = undefined;
    this.supportTeamCode = undefined;
    this.recipient = undefined;
    this.recipientCode = undefined;
    this.ticketCategory = undefined;
    this.ticketCategoryCode = undefined;
    this.deliveryDate = undefined;
    this.deliveryTime = undefined;
    this.deliveryDateCounterStop = undefined;
    this.deliveryDateManual = undefined;
    this.deliveryDateEval = undefined;
    this.ticketValue = undefined;
    this.currency = undefined;
    this.orderNumber = undefined;
    this.em30Percent = undefined;
    this.em30Currency = undefined;
    this.em70Percent = undefined;
    this.em70Currency = undefined;
    this.newContentString = undefined;
    this.nextStates = [];
    this.notes = [];
    this.docs = [];
    this.associatedPartners = [];
    

    this.getId = function () {
      return this.ticketId;
    };
      
    this.getDescription = function () {
      return this.description;
    };
      
    this.getNotes = function () {
      return this.notes;
    };
      
    this.getDocs = function () {
      return this.docs;
    };
      
    this.getNextStates = function () {
      return this.nextStates;
    };
      
    this.getAssociatedPartners = function () {
      return this.associatedPartners;
    };
      
    this.setDescription = function (description) {
      this.description = description;
    };
      
    this.setNotes = function (notes) {
        
      this.notes = notes;
    };
      
    this.addNewNote = function (note) {
        
      this.notes.push(note);
    };
      
    this.setPriorityCode = function (code) {
        
      this.priorityCode = code;
    };
      
    this.setDocs = function (docs) {
      this.docs = docs;
    };
      
    this.setAssociatedPartners = function (newPartners) {
      this.associatedPartners = newPartners;
    };
      
    this.setNextStates = function (newStates) {
      this.nextStates = newStates;
    };
      
    this.setNewStatus = function (newState) {
      this.status = newState;
    };
      
    this.setNewStatusCode = function (newCodeState) {
      this.statusCode = newCodeState;
    };
      
    this.addNewPartners = function (partners){
        if(this.associatedPartners.length>0){
            if(partners.length>0){
                for(var i = 0; i<partners.length; i++){
                    this.associatedPartners.push(partners[i]);
                }
            }
            
        }else{
            this.associatedPartners = partners;
        }
    },
        
    this.addNewPartner = function (partner){
        
            if(partner){
                this.associatedPartners.push(partner);              
            }
    },

    this.getModel = function () {

      var model = new sap.ui.model.json.JSONModel(this);
      return model;

    };


    this.update = function (data) {
      for (var prop in data) {
        this[prop] = data[prop];
      }
    };

    if (serializedData) {
      this.update(serializedData);
    }
    return this;
  };
  return Ticket;


})();
