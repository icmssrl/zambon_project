jQuery.sap.declare("icms.Component");
jQuery.sap.require("icms.MyRouter");


sap.ui.core.UIComponent.extend("icms.Component", {
	metadata: {
		config: {
			resourceBundle: "custom/i18n/text.properties",

			//puntati dalla classe settings.core [custom/settins]
      settings: {
				lang: "IT",
				isMock:false,
//				serverUrl:"proxy/sap/opu/odata/sap/Z_SSM_SRV/",
				serverUrl:"http://zagitvg160.zip.zg:8000/sap/opu/odata/sap/Z_SSM_SRV/",
//				serverUrl:"http://zagitvg160:8000/sap/opu/odata/sap/Z_SSM_SRV/",
//                logoffUrl:"proxy/sap/public/bc/icf/logoff"
                logoffUrl:"http://zagitvg160.zip.zg:8000/sap/public/bc/icf/logoff"
//                logoffUrl:"http://zagitvg160:8000/sap/public/bc/icf/logoff"
			},

			//rootView: 'view.App'
		},
		includes : [  //importare css di custom e lib
			"css/custom.css",
			"libs/lodash.js",
			"libs/q.js",
			],
		dependencies: {
			libs: [
				"sap.m",
				"sap.ui.layout"
				]
		},

		routing: {
			config: {
				viewType: "XML",
				viewPath: "view",
				clearTarget: false,
				// targetControl: "app",
				transition: "slide",
				// targetAggregation: "detailPages",
			},

			routes:
			[
				{
					name: "login",
					pattern: "",
					view: "Login",
					viewId: "loginId",
					targetAggregation : "pages",
					targetControl:"app"
				},
                {
					name: "launchpad",
					pattern: "launchpad",
					view: "Launchpad",
					viewId: "launchpadId",
					targetAggregation:"pages",
					targetControl:"app"
				},
                {
					name: "userInfo",
					pattern: "userInfo",
					view: "UserInfo",
					viewId: "userInfoId",
					targetAggregation:"pages",
					targetControl:"app"
				},
                {
					name: "masterMobile",
					pattern: "masterMobile",
					view: "TicketList",
					viewId: "ticketListId",
					targetAggregation:"pages",
					targetControl:"app"
				},
                {
					pattern:"split",
					name:"splitApp",
					view:"SplitApp",
					viewType:"JS",
					targetControl:"app",
					targetAggregation:"pages",
					subroutes:
					[
						{
							name: "ticketList",
							pattern: "ticketList",
							view: "TicketList",
							viewId: "ticketListId",
							targetAggregation:"masterPages",
							targetControl:"splitApp",
							subroutes:
							[
								{
									name: "ticketDetail",
									pattern: "ticket/detail/{id}",
									view: "TicketDetail",
									viewId: "ticketDetailId",
									targetAggregation:"detailPages",
									targetControl:"splitApp"
								},
//								{
//									name: "ticketEdit",
//									pattern: "ticket/edit/{id}",
//									view: "TicketEdit",
//									viewId: "ticketEditId",
//									targetAggregation:"detailPages",
//									targetControl:"splitApp"
//								},
                                {
									name: "noDataSplitDetail",
									pattern: "ticket/:empty:",
									view: "NoDataSplitDetail",
									viewId: "noDataSplitDetailId",
									targetAggregation:"detailPages",
									targetControl:"splitApp"
								}

							]
					 	}

					]
				},
                {
                    name: "ticketReport",
					pattern: "ticketReport/:id:",
					view: "TicketReport",
					viewId: "ticketReportId",
					targetAggregation:"pages",
					targetControl:"app"
                },
				{
					name: "newUser",
					pattern: "user/new",
					view: "UserCreate",
					viewId: "newUserId",
					targetAggregation:"pages",
					targetControl:"app"

				}
			]

	}
},

	/**
	 * !!! The steps in here are sequence dependent !!!
	 */
	init: function() {

		//il ns icms va cambiato a seconda del nome app e nome cliente
		jQuery.sap.registerModulePath("icms", "custom");
		jQuery.sap.registerModulePath("view", "custom/view");
		jQuery.sap.registerModulePath("model", "custom/model");
		jQuery.sap.registerModulePath("utils", "custom/utils");

		// 2. call overridden init (calls createContent)
		sap.ui.core.UIComponent.prototype.init.apply(this, arguments);

		var oI18nModel = new sap.ui.model.resource.ResourceModel({
			bundleUrl: [this.getMetadata().getConfig().resourceBundle]
		});
		this.setModel(oI18nModel, "i18n");
        if (sessionStorage.getItem("language")){
            sap.ui.getCore().getConfiguration().setLanguage(sessionStorage.getItem("language"));
        }
        //sap.ui.getCore().setModel(oI18nModel, "i18n");

		// 3a. monkey patch the router
		jQuery.sap.require("sap.m.routing.RouteMatchedHandler");
		var router = this.getRouter();
		router.myNavBack = icms.MyRouter.myNavBack;
		router.myNavToWithoutHash = icms.MyRouter.myNavToWithoutHash;
		icms.MyRouter.router = router;

		this.routeHandler = new sap.m.routing.RouteMatchedHandler(router);
		router.initialize();


		// set device model
		var oDeviceModel = new sap.ui.model.json.JSONModel({
			isTouch : sap.ui.Device.support.touch,
			isNoTouch : !sap.ui.Device.support.touch,
			isPhone : sap.ui.Device.system.phone,
			isNoPhone : !sap.ui.Device.system.phone,
			listMode : (sap.ui.Device.system.phone) ? "None" : "SingleSelectMaster",
			listItemType : (sap.ui.Device.system.phone) ? "Active" : "Inactive"
		});
		oDeviceModel.setDefaultBindingMode("OneWay");
		this.setModel(oDeviceModel, "device");

		var appStatusModel  = new sap.ui.model.json.JSONModel();
        appStatusModel.setProperty("/navBackVisible", false);
		this.setModel(appStatusModel,"appStatus" );

	},

	destroy: function() {
		if (this.routeHandler) {
			this.routeHandler.destroy();
		}

		// call overridden destroy
		sap.ui.core.UIComponent.prototype.destroy.apply(this, arguments);
	},

	createContent: function() {
		// create root view
		var oView = sap.ui.view({
			id: "mainApp",
			viewName: "view.App",
			type: "JS",
			viewData: {
				component: this
			}
		});

		return oView;
	}





});
