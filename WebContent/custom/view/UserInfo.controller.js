jQuery.sap.require("sap.m.MessageBox");

jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.i18n");



view.abstract.AbstractController.extend("view.UserInfo", {

  onInit: function () {
    this.router = sap.ui.core.UIComponent.getRouterFor(this);
    this.router.attachRoutePatternMatched(this.handleRouteMatched, this);

    this.editPass = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.editPass, "editPass");


    this.userModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.userModel, "userModel");
    //this.userModel.setProperty("/modPass", false);

  },

  handleRouteMatched: function (evt) {

    var userInfo = model.persistence.Storage.session.get("user");
    this.userModel.setData(userInfo);
    this.userModel.setProperty("/modPass", false);
    this.userModel.setProperty("/confirmChange", false);
    this.editPass.setProperty("/passOld", "")
    this.editPass.setProperty("/passNew", "")
    this.editPass.setProperty("/passNewConfirm", "")

    var name = evt.getParameter("name");
    if (name !== "userInfo") {
      return;
    };


    this.passInsertOld = false;
    this.passInsertNew = false;
    this.passInsertNewConfirm = false;
  },

  navBack: function () {
    this.router.myNavBack();
    this.editPass.setProperty("/passOld", "");
    this.editPass.setProperty("/passNew", "");
    this.editPass.setProperty("/passNewConfirm", "");
    this.userModel.setProperty("/confirmChange", false);
    this.userModel.setProperty("/modPass", false);
  },

  modifyPassword: function () {
    if (this.userModel.getProperty("/modPass") === false) {
      this.userModel.setProperty("/modPass", true);
    } else {
      this.userModel.setProperty("/modPass", false);
    };
  },

  liveChange1: function (evt) {
    if (evt.getParameters().value !== "") {
      this.passInsertOld = true;
    } else {
      this.passInsertOld = false;
    };
    if (this.passInsertOld === true && this.passInsertNew === true && this.passInsertNewConfirm === true) {
      this.userModel.setProperty("/confirmChange", true);
    } else {
      this.userModel.setProperty("/confirmChange", false);
    };
  },

  liveChange2: function (evt) {
    if (evt.getParameters().value !== "") {
      this.passInsertNew = true;
    } else {
      this.passInsertNew = false;
    };
    if (this.passInsertOld === true && this.passInsertNew === true && this.passInsertNewConfirm === true) {
      this.userModel.setProperty("/confirmChange", true);
    } else {
      this.userModel.setProperty("/confirmChange", false);
    };
  },

  liveChange3: function (evt) {
    if (evt.getParameters().value !== "") {
      this.passInsertNewConfirm = true;
    } else {
      this.passInsertNewConfirm = false;
    };
    if (this.passInsertOld === true && this.passInsertNew === true && this.passInsertNewConfirm === true) {
      this.userModel.setProperty("/confirmChange", true);
    } else {
      this.userModel.setProperty("/confirmChange", false);
    };
  },

  //    confirmChange: function () {
  //        var passOld = this.editPass.getData().passOld;
  //        var passNew = this.editPass.getData().passNew;
  //        var passConfirm = this.editPass.getData().passNewConfirm;
  //        if (passOld !== $.parseJSON(atob(localStorage.getItem("user"))).p) {
  //            sap.m.MessageBox.alert(this._getLocaleText("passOldDiff"), this._getLocaleText("avvisoPass"));
  //        } else {
  //            if (passOld === passNew) {
  //                sap.m.MessageBox.alert(this._getLocaleText("passUgualiOldNew"), this._getLocaleText("avvisoPass"));
  //            } else {
  //                if (passNew !== passConfirm) {
  //                    sap.m.MessageBox.alert(this._getLocaleText("passDiverse"), this._getLocaleText("avvisoPass"));
  //                } else {
  //                    this.userModel.setProperty("/modPass", false);
  //
  //
  //        this.editPass.setProperty("/passOld", "");
  //        this.editPass.setProperty("/passNew", "");
  //        this.editPass.setProperty("/passNewConfirm", "");
  //        this.userModel.setProperty("/confirmChange", false);
  //   }

});
