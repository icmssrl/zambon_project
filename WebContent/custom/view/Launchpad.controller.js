jQuery.sap.require("model.Tiles");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.i18n");
//jQuery.sap.require("model.User");

view.abstract.AbstractController.extend("view.Launchpad", {

  onInit: function () {
    this.router = sap.ui.core.UIComponent.getRouterFor(this);
    this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    // view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

    this.tileModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.tileModel, "tiles");

    this.userModel = new sap.ui.model.json.JSONModel();
    this.getView().setModel(this.userModel, "userModel");


  },

  handleRouteMatched: function (oEvent) {


    var oParameters = oEvent.getParameters();

    if (oParameters.name !== "launchpad") {
      return;
    }
    var appModel = this.getView().getModel("appStatus");

    this.user = model.persistence.Storage.session.get("user");
    this.userModel.setData(this.user);
    this.refreshTiles(this.user);
  },





  // onLogoutPress: function (oEvent) {
  //   this.doLogout();
  // },
  //
  // doLogout: function () {
  //   //resetto dati
  //   sessionStorage.clear();
  //   this.router.navTo("login");
  // },

  onTilePress: function (oEvent) {
    //recupero la tile
    var tilePressed = oEvent.getSource().getBindingContext("tiles").getObject();
    var url = tilePressed.url;
    //lancio l'app
    var device = this.getView().getModel("device").getData();
    if(device.isPhone ===true){
        this.router.navTo("masterMobile");
    }else{
        
        this.launchApp(url);
    }
    
    
    
  },

  onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");
    },

  launchApp: function (url) {
    this.router.navTo(url);
  },

  //**
  refreshTiles: function(user)
  {
      var userType;
      if(user.userType === "ZCONS"){
          userType = "user";
      }else{
          userType = "admin";
      }

    var tile = model.Tiles.getMenu(userType);
    this.tileModel.setData(tile);
    this.tileModel.refresh();
  },

  onLinkToUserInfoPress: function(evt){
      this.router.navTo("userInfo");
  }




});
