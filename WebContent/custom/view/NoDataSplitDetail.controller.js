jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("view.abstract.AbstractController");


view.abstract.AbstractController.extend("view.NoDataSplitDetail", {



	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);

	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var name = evt.getParameter("name");
        
       

		if ( name !== "noDataSplitDetail" ){
			return;
		}

		this.user = model.persistence.Storage.session.get("user");
        this.userModel = new sap.ui.model.json.JSONModel(this.user);
        this.getView().setModel(this.userModel, "userModel");




	},

});
