jQuery.sap.require("model.User");
jQuery.sap.require("model.i18n");
jQuery.sap.require("view.abstract.AbstractController");
jQuery.sap.require("sap.m.MessageToast");
jQuery.sap.require("sap.m.MessageBox");

view.abstract.AbstractController.extend("view.Login", {

  onExit: function () {

  },


  onInit: function () {
    view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
    // this.router = sap.ui.core.UIComponent.getRouterFor(this);
    // this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
    this.credentialModel = new sap.ui.model.json.JSONModel();

  },


  handleRouteMatched: function (evt) {

    view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
    var name = evt.getParameter("name");
    
    var browser=this.get_browser_info();
    console.log(browser.name);
    console.log(browser.version);
//    var loginForm = this.getView().byId("loginSimpleFormId");
//      
//    if (browser && browser.name !== ""){
//        if(browser.name === "IE"){
//            loginForm.addStyleClass("IEstyleClass");
//            document.getElementById("loginId--loginSimpleFormId").className = "IEstyleClass";
//        }
//    }

    if (name !== "login") {
      return;
    }

    this.user = new model.User();
    //var credentialModel = this.user.getNewCredential();
    this.getView().setModel(this.credentialModel, "usr");

    model.i18n.setModel(this.getView().getModel("i18n"));
    


  },
    
    
  get_browser_info: function (){
    var ua=navigator.userAgent,tem,M=ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
    if(/trident/i.test(M[1])){
        tem=/\brv[ :]+(\d+)/g.exec(ua) || []; 
        return {name:'IE ',version:(tem[1]||'')};
        }   
    if(M[1]==='Chrome'){
        tem=ua.match(/\bOPR\/(\d+)/)
        if(tem!=null)   {return {name:'Opera', version:tem[1]};}
        }   
    M=M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem=ua.match(/version\/(\d+)/i))!=null) {M.splice(1,1,tem[1]);}
    return {
      name: M[0],
      version: M[1]
    };
 },
 
  


//  onLoginPress: function (evt) {
////  onLoginPress: function (evt) {
////    var username = this.getView().getModel("usr").getProperty("/username");
//
////    var pwd = this.getView().getModel("usr").getProperty("/password");
////
////    this.user.setCredential(username, pwd);
////    var doLogin = _.bind(this.user.doLogin, this);
//
////    var choosePath = _.bind(this.choosePath, this);
////
////    this.chiamataOData(username, pwd)
////      .then(doLogin(this.user.getCredential().username, this.user.getCredential().pwd)
////        .then(_.bind(
////          function (result) {
////
//////            if (result.organizationData && _.chain(result.organizationData.results).pluck('division').uniq().value().length === 1) {
//////              this.router.navTo("launchpad");
//////            } else {
//////              if (_.chain(result.organizationData.results).pluck('division').uniq().value().length > 1) {
//////                this.router.navTo("soLaunchpad");
//////              }
//////            }
//////          }
////          this.router.navTo("soLaunchpad");
////          }, this)));
//        this.router.navTo("pmScheduling");
//
//  },

 onAfterRendering : function(evt)
	{
			view.abstract.AbstractController.prototype.onAfterRendering.apply(this, arguments);
		$(document).on("keypress", _.bind(function(evt)
	{
		if(evt.keyCode === 13 && (this.validateCheck()))  
		{
			this.onLoginPress();
		}

	}, this));
        

	},
    
    
    chiamataOData: function (user, pwd) {
    var defOdata = Q.defer();
    fError = function (err) {
      sap.m.MessageBox.alert("Utente SAP non trovato", {
        title: "Attenzione"
      });
      defOdata.reject(err);
    };
    fSuccess = function () {
      defOdata.resolve();
      console.log("credenziali sap");
    };

    var success = _.bind(fSuccess, this);
    var error = _.bind(fError, this);

    //var tok = "ICMSDEV" + ':' + "icms2016";
    var tok = user + ":" + pwd;
    var auth = btoa(tok);

    $.ajaxSetup({
      headers: {
        "Authorization": "Basic " + auth,
        "X-CSFR-TOKEN": "fetch"
      }
    });
    var url = icms.Component.getMetadata().getConfig().settings.serverUrl;
    $.ajax({
      url: url,
      type: "GET",
      success: success,
      error: error,
      async: false
    });

    return defOdata.promise;
  },

	onLoginPress:function(evt)
	{
        
    this.onLogoutPress();
    
    //&this.chiamataOData();
        var username = this.getView().getModel("usr").getProperty("/username");
		var pwd = this.getView().getModel("usr").getProperty("/password");
		
		this.user.setCredential(username, pwd);
        
        var doLogin = _.bind(this.user.doLogin, this);
        
        this.chiamataOData(username, pwd)
        .then(doLogin(this.user.getCurrentUser().username) //this.user.getCredential().username, this.user.getCredential().password
        .then(_.bind(
          function (result) {

            this.user.update(result);   
//            if(this.user.userType !== "ZCONS"){
//                this.appModel.setProperty("/isSuperUser", true);
//            }else{
//                this.appModel.setProperty("/isSuperUser", false);
//            }
            $(document).off("keypress");
            this.router.navTo("launchpad");
     

          }, this)));

//		 this.user.doLogin()
//		 .then(_.bind(function(result){
//		 	this.router.navTo("launchpad");
//		
//		 }, this));




	},
    
    onSwitchLanguage: function (oEvent) {
    var control = oEvent.getSource();
    var buttonID = control.getId();
    var oI18nModel;
    switch (buttonID) {
    case "loginId--italyFlag":

      sap.ui.getCore().getConfiguration().setLanguage("it_IT");
      sessionStorage.setItem("language", "it_IT");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      location.reload();

      break;
    case "loginId--britainFlag":
      sap.ui.getCore().getConfiguration().setLanguage("en_EN");
      sessionStorage.setItem("language", "en_EN");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      location.reload();
      break;
    case "loginId--usaFlag":
      sap.ui.getCore().getConfiguration().setLanguage("en_US");
      sessionStorage.setItem("language", "en_EN");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      location.reload();
      break;
    case "loginId--spainFlag":
      sap.ui.getCore().getConfiguration().setLanguage("en_EN");
      sessionStorage.setItem("language", "en_EN");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      break;
    case "loginId--germanyFlag":
      sap.ui.getCore().getConfiguration().setLanguage("en_EN");
      sessionStorage.setItem("language", "en_EN");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      break;
    case "loginId--franceFlag":
      sap.ui.getCore().getConfiguration().setLanguage("en_EN");
      sessionStorage.setItem("language", "en_EN");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });
      break;
    default:
      sap.ui.getCore().getConfiguration().setLanguage("it_IT");
      sessionStorage.setItem("language", "it_IT");
      this.getView().getModel("i18n").refresh();
      sap.m.MessageToast.show(model.i18n._getLocaleText("LANGUAGE_CHANGED"), {
        at: "center top"
      });



    }
 
  }

  
//  chiamataOData: function (user, pwd) {
//    var defOdata = Q.defer();
//    fError = function () {
//      sap.m.MessageBox.alert("Utente SAP non trovato", {
//        title: "Attenzione"
//      });
//    };
//    fSuccess = function () {
//      defOdata.resolve();
//      console.log("credenziali");
//    };
//
//    var success = _.bind(fSuccess, this);
//    var error = _.bind(fError, this);
//
//    var tok = "SYSTEM" + ':' + "Saphdb_40$";
//    //var tok = user + ":" + pwd;
//    var auth = btoa(tok);
//
//    $.ajaxSetup({
//      headers: {
//        "Authorization": "Basic " + auth,
//        "X-CSFR-TOKEN": "fetch"
//      }
//    });
//    var url = "http://gspidev.icms.it:8000/GSPI/odata/odata.xsodata";
//    $.ajax({
//      url: url,
//      type: "GET",
//      success: success,
//      error: error,
//      async: false
//    });
//
//    return defOdata.promise;
//  },
  

});