jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("utils.Busy");
jQuery.sap.require("model.persistence.Storage");
jQuery.sap.require("model.persistence.Serializer");
jQuery.sap.require("model.ui.CustomRow");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("utils.Collections");
jQuery.sap.require("model.collections.Tickets");
jQuery.sap.require("model.Ticket");
jQuery.sap.require("model.collections.Partners");
jQuery.sap.require("model.Partner");
jQuery.sap.require("model.Current");
jQuery.sap.require("utils.ParseDate");
jQuery.sap.require("model.i18n");
jQuery.sap.require("model.collections.NextStates");

view.abstract.AbstractController.extend("view.TicketDetail", {

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);



	},


	handleRouteMatched: function (evt) {

		view.abstract.AbstractController.prototype.handleRouteMatched.apply(this, arguments);
		var routeName = evt.getParameter("name");

		if ( routeName !== "ticketDetail"){
			return;
		}

        this.inputPartnerRoleCode = undefined;
        this.inputPartnerRole = undefined;

        if(this.addPartnersDialog){
            this.addPartnersDialog.destroy();
        }

		this.id = evt.getParameters().arguments.id;
        this.appModel = this.getView().getModel("appStatus");

//        this.appModel.setProperty("/setEditable", false);

        ///********In ottica update**************/////////

        this.appModel.setProperty("/editMode", false);
        this.appModel.setProperty("/categoryCanChange", false);
        var switchModeButton = this.getView().byId("editButton");
        var endChangesButton = this.getView().byId("endChangesButton");
        var assignStatusButton = this.getView().byId("assignStatusButton");
        endChangesButton.setVisible(false);
        assignStatusButton.setVisible(false);
        switchModeButton.setVisible(true);

        ///********In ottica update**************/////////

        var iconTabBar = this.getView().byId("iconTabBarId");
        iconTabBar.setSelectedKey("description");
        if(iconTabBar.getSelectedKey() === "status"){
            
            var editMode = this.appModel.getProperty("/editMode");

            var assignStatusButton = this.getView().byId("assignStatusButton");

            if (editMode === true)
                assignStatusButton.setVisible(true);
               
            else{
                assignStatusButton.setVisible(false);

            }


        }

        this.user = model.persistence.Storage.session.get("user");
        this.userModel = new sap.ui.model.json.JSONModel(this.user);
        this.getView().setModel(this.userModel, "userModel");


        model.collections.Tickets.getById(this.id)
		.then(_.bind(function(res){
            console.log(res);
            this.ticket = res;
            this.refreshView(res);
            this.currentStatusCode = res.statusCode;
//            this.setNewTicketStates(res, this.id);
            this.setNewTicketStates();
        }, this));

        if(this.ticket && this.ticket.statusCode && this.ticket.statusCode === "E0029"){
            this.appModel.setProperty("/categoryCanChange", true);
        }
        this.loadOdataSets();



	},


	refreshView : function(data)
	{
        this.getView().setModel(data.getModel(), "t");

	},

    refreshPartnerList: function(data){
        this.partnerListModel = new sap.ui.model.json.JSONModel();
//        var partnersArr = this.ticket.associatedPartners;

//        data = data.filter(_.bind(function(val) {
//          return this.ticket.associatedPartners.indexOf(val) == -1;
//        }, this));
        var p = {partners:[]};
        p.partners = data;
        this.partnerListModel.setData(p);
        this.getView().setModel(this.partnerListModel, "p");
    },



    loadOdataSets: function () {
    var pToSelArr = [
      {
        "type": "SSM_Priority",
        "namespace": "priority"
      },
      {
        "type": "SSM_Status",
        "namespace": "status"
      },
      {
        "type": "SSM_Category",
        "namespace": "category"
      },
      {
        "type": "SSM_PartnerList",
        "namespace": "partners"
      },
      {
        "type": "SSM_PartnerFCTList",
        "namespace": "partnerRole"
      }
    ];

    _.map(pToSelArr, _.bind(function (item) {
      utils.Collections.loadOdataSets(item.type)
        .then(_.bind(function (result) {
          this.getView().setModel(result, item.namespace);

        }, this))
    }, this));
  },


    onPost: function (oEvent) {
//			var oDate = new Date();
//			var sDate = utils.ParseDate().formatDate(oDate, "dd/MM/yyyy");
			// create new entry
			var sValue = oEvent.getParameter("value");
			var oEntry = {
//				Author : this.user.username,
				noteType : this.getView().getModel("t").getData().ticketCategory,
				ticketId : this.getView().getModel("t").getData().ticketId,
				noteDescription : sValue
			};

			// update model
			var oModel = this.getView().getModel("t");
			var aEntries = oModel.getData().notes;
			aEntries.unshift(oEntry);
			oModel.refresh();
		},

    onSenderPress: function (oEvent) {
			sap.m.MessageToast.show("Clicked on Link: " + oEvent.getSource().getSender());
	},

    onIconPress: function (oEvent) {
			sap.m.MessageToast.show("Clicked on Image: " + oEvent.getSource().getSender());
	},


    onIconTabClicked: function(event){
        var assignStatusButton = this.getView().byId("assignStatusButton");
        var source = event.getSource();
        var selectedKey = source.getSelectedKey();
        if(selectedKey === "status"){
            var editMode = this.appModel.getProperty("/editMode");
            if (editMode === true)
                assignStatusButton.setVisible(true);
            else{
                assignStatusButton.setVisible(false);
            }
        }else{
            assignStatusButton.setVisible(false);
        }

    },

    switchToEditMode: function(evt){
        var source = evt.getSource();
        var switchModeButton = this.getView().byId("editButton");
        var assignStatusButton = this.getView().byId("assignStatusButton");
        var iconTabBar = this.getView().byId("iconTabBarId");
        if(iconTabBar.getSelectedKey() === "status"){
            assignStatusButton.setVisible(true);
        }
        var endChangesButton= this.getView().byId("endChangesButton");
        var addContentTextArea = this.getView().byId("newContent");
        addContentTextArea.setValue("");
        this.appModel.setProperty("/editMode", true);
        switchModeButton.setVisible(false);
        this.getView().getModel("t").refresh();
    },
    
    returnToViewModeWithoutSaving: function(){
    	var idTicket = this.id;
    	var switchModeButton = this.getView().byId("editButton");
        var assignStatusButton = this.getView().byId("assignStatusButton");
        var exitWithoutSaving = this.getView().byId("endChangesWithoutSavingButton");
    	 model.collections.Tickets.getById(idTicket)
	         .then(_.bind(function(res){
//	             console.log(res);
	             this.ticket = res;
	             this.refreshView(res);
	             //this.setNewTicketStates(res, idTicket);
	             this.setNewTicketStates();
	             this.appModel.setProperty("/editMode", false);
	             switchModeButton.setVisible(true);
	             assignStatusButton.setVisible(false);
	             exitWithoutSaving.setVisible(false);
	             this.getView().getModel("t").refresh();
	     }, this));
    },

    returnToViewMode: function(){

        ////UPDATE
        this.ticket.newContentString = this.getView().byId("newContent").getValue();
        var serializeTicketBack = _.bind(model.persistence.Serializer.ticket.toSAP, this);
        
        var ticketToUpdate = jQuery.extend(true, {}, this.ticket);
        for(var prop in ticketToUpdate){
            if(_.isFunction(ticketToUpdate[prop])){
                delete ticketToUpdate[prop];
            }
        }
        console.log(ticketToUpdate);
        var ticketSerialized = serializeTicketBack(ticketToUpdate);
        var idTicket = this.id;
        var that = this;
        var masterView = sap.ui.getCore().byId("ticketListId--ticketListPageId");
        var detailView = this.getView().byId("detailPageId");
        
        ////*************UPDATE********************/////////
        
        masterView.setBusy(true);
        detailView.setBusy(true);
        
        var fSuccess  = function(responseFromOdata){
            
            masterView.setBusy(false);
            detailView.setBusy(false);

            var switchModeButton = this.getView().byId("editButton");
            var assignStatusButton = this.getView().byId("assignStatusButton");

            this.appModel.setProperty("/editMode", false);

            switchModeButton.setVisible(true);
            assignStatusButton.setVisible(false);
            this.getView().getModel("t").refresh();
            sap.m.MessageToast.show(model.i18n._getLocaleText("CHANGES_APPLIED"));
 
            var eventBus = sap.ui.getCore().getEventBus();
            if(sessionStorage.getItem("filtersAppliedOnMaster") === true){
                eventBus.publish("loadFilteredMasterList", "fireloadFilteredMasterList");
            }else{
                eventBus.publish("loadMasterList", "fireloadMasterList");
            }
            
            
       
            
//            model.collections.Tickets.loadTickets(this.user)
//            .then(_.bind(function(resMaster){
//                console.log(resMaster);
//                sap.ui.getCore().byId("ticketListId").getModel("tickets").setData({"tickets": resMaster});
//                sap.ui.getCore().byId("ticketListId").getModel("tickets").updateBindings();
//                sap.ui.getCore().byId("ticketListId").getModel("tickets").refresh();
//                model.Ticket().getModel().updateBindings();
//                var master = sap.ui.getCore().byId("splitApp-Master").getAggregation("pages")[0].getContent();
//                master.getModel("tickets").refresh();
//                master.rerender();  
            
                model.collections.Tickets.getById(idTicket)
                    .then(_.bind(function(res){
                        console.log(res);
                        this.ticket = res;
                        this.refreshView(res);
                        //this.setNewTicketStates(res, idTicket);
                        this.setNewTicketStates();
                }, this));
            
//            model.collections.Tickets.getModel().refresh(true);
                
//            }, this));  
            
            
            
            

        };
        fSuccess = _.bind(fSuccess, this);
        
        
        var fError  = function(err){
            masterView.setBusy(false);
            detailView.setBusy(false);
            console.log("Error -- UPDATE FAILED!");
            sap.m.MessageToast.show(err.response.statusCode+": "+err.response.statusText + "\n"+ err.message);

        };

        fError = _.bind(fError, this);
 

      model.odata.chiamateOdata.updateTicket(idTicket, ticketSerialized,fSuccess, fError);

        /////**************************************////////

       
    },
    
    setNewTicketStates: function(){
      model.collections.NextStates.getNextStates(this.currentStatusCode)
      .then(_.bind(function(res){
          console.log(res);
          this.ticket.setNextStates(res);
      }, this));

    },

//    setNewTicketStates: function(ticket, idTicket){
////        var deferId= Q.defer();
//        var fSuccess  = function(result){
//            if(result)
//            if(result.results && result.results.length>0){
////                  console.log(result.results);
//                var ticketArray = [];
//                for(var i = 0; i<result.results.length; i++){
//                    var data = model.persistence.Serializer.status.fromSAP(result.results[i]);
//                    ticketArray.push(data);
//                }
//                this.ticket.setNextStates(ticketArray);
//
////                  deferId.resolve(result); //
//             }else{
//                  console.log("Status not found!");
//             }
//
//        };
//        fSuccess = _.bind(fSuccess, this);
//
//        var fError  = function(err){
//
//            console.log("Error -- Status not found!");
////            deferId.reject(err);
//        };
//
//        fError = _.bind(fError, this);
//
//      model.odata.chiamateOdata.getNextStates(idTicket, fSuccess, fError);
//
//    },

    handleComboBoxPriorityChange: function(evt){
        var src = evt.getSource();
        var selectedItem = src.getValue();
        var priorityCodeValue = selectedItem.charAt(0);
        this.ticket.setPriorityCode(priorityCodeValue);
    },

    onOpenAssignActionSheetPress: function(oEvent){

			var oButton = oEvent.getSource();

            var nextStates = this.ticket.nextStates;
            var buttons = _.bind(this.createStatusButtons, this);
            var button;
			// create action sheet only once
			if (!this._actionSheet) {
				this._actionSheet = sap.ui.xmlfragment(
					"view.fragment.assignStatusActionSheet",
					this
				);
				this.getView().addDependent(this._actionSheet);
			}
            if(this._actionSheet.getButtons().length > 0)
            this._actionSheet.removeAllButtons();
            for(var i = 0; i < nextStates.length; i++){
                button = buttons(nextStates[i]);
                this._actionSheet.addButton(button);
            }

			this._actionSheet.openBy(oButton);

    },

    createStatusButtons: function(b){
        var newButton = new sap.m.Button(
            {
                text: b.statusCode+" - "+b.statusText ,
                class: "assignStatusButtonClass",
                press: [this.assignNextStatusButton, this]
            }
        );
        return newButton;
    },

    assignNextStatusButton: function(evt){
        var source = evt.getSource();
        var newStatus = source.getText();
        this.ticket.setNewStatus(newStatus.substring(8));
        this.ticket.setNewStatusCode(newStatus.substr(0,5));
        var data = this.getView().getModel("t").getData();
        this.ticket.update(data);
        this.getView().getModel("t").refresh();
//        this.returnToViewMode();
    },

    handleRemovePartner: function(evt){
        var position = evt.getParameters().listItem;
        var oContext = position.getBindingContext("t");
        var oModel = oContext.getModel();
        var row = oContext.getObject();
//        var positionId = row.getId();
//        this.order.removePosition(positionId);
        var oData = oModel.getData();
        var currentRowIndex = parseInt(oContext.getPath().split('/')[oContext.getPath().split('/').length - 1]);
        oData.associatedPartners.splice(currentRowIndex,1);
        oModel.updateBindings();
        this.getView().getModel("t").refresh(true);
    },

    keyUpFunc: function(e) {
          if (e.keyCode == 27) {
                     // codice per il pulsante escape per evitare che lo user chiuda il dialog via ESC

                    if(this.addPartnersDialog){
                            this.addPartnersDialog.destroy();
                    }
                    if(this.uploadDialog){
                            this.uploadDialog.destroy();
                    }
                    if(this.descriptionDialog){
                            this.descriptionDialog.destroy();
                    }
                    if(this.newPartnerSelectDialog){
                            this.newPartnerSelectDialog.destroy();
                    }
                    if(this._valueHelpPartnerRoleDialog){
                            this._valueHelpPartnerRoleDialog.destroy();
                    }
                    if(this.newNoteDialog){
                            this.newNoteDialog.destroy();
                    }


                    $(document).off("keyup");

                    }
    },
    
    ////////*********Add new note functions**************////////////
    
    openAddNewNoteDialog: function(evt){
        if(!this.newNoteDialog)
        this.newNoteDialog = sap.ui.xmlfragment("view.dialog.addNewSapNote", this);
        var page = this.getView().byId("detailPageId");
        page.addDependent(this.newNoteDialog);
        var noteNr = sap.ui.getCore().byId("addNoteIdentifier");
        var shortDescription = sap.ui.getCore().byId("shortNoteDescription");
        noteNr.setValue("");
        shortDescription.setValue("");
        this.newNoteDialog.open();


        $(document).keyup(_.bind(this.keyUpFunc, this));
    },
    
    onCancelNewNotePress: function(evt){
        if(this.newNoteDialog){
            this.newNoteDialog.close();
        }
        sap.m.MessageToast.show(model.i18n._getLocaleText("NO_NOTES_ADDED"));

    },
    
    onConfirmNewNotePress: function(evt){
        var noteNr = sap.ui.getCore().byId("addNoteIdentifier");
        var shortDescription = sap.ui.getCore().byId("shortNoteDescription");
        var newNote = {
          noteId: "",  
          noteDescription: "",  
          ticketId: ""  
        };
        if(noteNr.getValue().trim() === "" || shortDescription.getValue().trim() === ""){
            sap.m.MessageToast.show(model.i18n._getLocaleText("INSERT_VALUES_FIRST"));
        }else{
            newNote.noteId = noteNr.getValue();
            newNote.noteDescription = shortDescription.getValue();
            newNote.ticketId = this.ticket.ticketId;
            this.ticket.addNewNote(newNote);
            this.getView().getModel("t").refresh();
            sap.m.MessageToast.show(model.i18n._getLocaleText("NEW_NOTE_ADDED"));
           if(this.newNoteDialog){
                this.newNoteDialog.close();
            }
        }


    },
    
    addNoteIdentifierLiveChange: function(evt){
        var shortDescription = sap.ui.getCore().byId("shortNoteDescription");
        shortDescription.setEnabled(true);
    },

    
    //////////////*****************************************///////////////
    
    
    /////***********Add new partner Functions*****************//////////////

    newPartnerSelect: function(evt){

        this.newPartner = new model.Partner();
        this.newPartnerSelectDialog = sap.ui.xmlfragment("view.dialog.newPartnerSelect", this);
        var page = this.getView().byId("detailPageId");
        page.addDependent(this.newPartnerSelectDialog);

            this.newPartnerSelectDialog.open();


        $(document).keyup(_.bind(this.keyUpFunc, this));
    },

    onCancelNewPartnerAddPress: function(evt){
        if(this.newPartnerSelectDialog){
            this.newPartnerSelectDialog.destroy();
        }
        sap.m.MessageToast.show(model.i18n._getLocaleText("NO_PARTNER_CHOSEN"));

    },

    onConfirmNewPartnerAddPress: function(evt){
        var partnerInput = sap.ui.getCore().byId("partnerInput");
        var checkBoxMainPartner = sap.ui.getCore().byId("mainPartnerChecked");
        var choosePartnerRoleInput = sap.ui.getCore().byId("choosePartnerRoleInput");

        if(choosePartnerRoleInput.getValue().trim() === "" || partnerInput.getValue().trim() === ""){
//            partnerInput.setEditable()
            sap.m.MessageToast.show(model.i18n._getLocaleText("INSERT_VALUES_FIRST"));
        }else{
            if(this.inputPartnerRoleCode && this.inputPartnerRole)
                this.newPartner.setPartnerFunctionCode(this.inputPartnerRoleCode);
                this.newPartner.setPartnerFunction(this.inputPartnerRole);
                this.newPartner.setMainPartner(checkBoxMainPartner.getSelected());
                this.ticket.addNewPartner(this.newPartner);
                this.getView().getModel("t").refresh();
            sap.m.MessageToast.show(model.i18n._getLocaleText("NEW_PARTNER_ADDED"));
           if(this.newPartnerSelectDialog){
                this.newPartnerSelectDialog.destroy();
            }
        }


    },

    choosePartnerRoleLiveChange: function(evt){
        var partnerInput = sap.ui.getCore().byId("partnerInput");
        partnerInput.setEnabled(true);
    },

    openAddPartnerDialog: function(evt){
        this.inputPartnerId = evt.getSource().getId();
        this.addPartnersDialog = sap.ui.xmlfragment("view.dialog.addPartnersDialog", this);
        var page = this.getView().byId("detailPageId");
        page.addDependent(this.addPartnersDialog);

            this.addPartnersDialog.open();



        $(document).keyup(_.bind(this.keyUpFunc, this));
    },

    handleSearchOnPartnerDialog: function(oEvent){
        var sValue = oEvent.getParameter("value");
         var partnerIdFilter=  new sap.ui.model.Filter({
                                    path: "partnerId",
                                    operator: "Contains",
                                    value1: sValue,
                                  });
        var partnerNameFilter=  new sap.ui.model.Filter({
                                    path: "partnerName",
                                    operator: "Contains",
                                    value1: sValue,
                                  });
        var partnerOrgNameFilter=  new sap.ui.model.Filter({
                                    path: "partnerOrgName",
                                    operator: "Contains",
                                    value1: sValue,
                                  });
        var partnerLastNameFilter=  new sap.ui.model.Filter({
                                    path: "partnerLastName",
                                    operator: "Contains",
                                    value1: sValue,
                                  });
        var partnerAddressFilter=  new sap.ui.model.Filter({
                                    path: "address",
                                    operator: "Contains",
                                    value1: sValue,
                                  });
        var partnerNrFilter=  new sap.ui.model.Filter({
                                    path: "partnerNr",
                                    operator: "Contains",
                                    value1: sValue,
                                  });
        var filters = [];
        filters.push(partnerIdFilter);
        filters.push(partnerNameFilter);
        filters.push(partnerOrgNameFilter);
        filters.push(partnerLastNameFilter);
        filters.push(partnerAddressFilter);
        filters.push(partnerNrFilter);

        var oFilter = new sap.ui.model.Filter({filters:filters, and:false});

        var oBinding = oEvent.getSource().getBinding("items");
        oBinding.filter([oFilter]);
    },

    handleConfirmOnPartnerDialog: function(oEvent){
        var aContexts = oEvent.getParameter("selectedContexts");


        if (aContexts.length) {
            var partner = {};
            sap.m.MessageToast.show(model.i18n._getLocaleText("PARTNER_CHOSEN") + " : " + aContexts.map(function(oContext) {
                partner=oContext.getObject();
                return oContext.getObject().address; }));
            var input = sap.ui.getCore().byId(this.inputPartnerId);
            if(input.getId()==="partnerInput"){
            input.setValue(partner.address);
            
            this.newPartner.update(partner);
            this.newPartner.ticketId = this.ticket.ticketId;
            }else{
                var inputValue = partner.partnerName +" "+ partner.partnerLastName +" "+ partner.partnerOrgName
                input.setValue(inputValue.trim());
            }

        }else{
            sap.m.MessageToast.show(model.i18n._getLocaleText("NO_PARTNER_CHOSEN"));
        }
        oEvent.getSource().getBinding("items").filter([]);

        this.addPartnersDialog.destroy();

    },

    handleCloseOnPartnerDialog: function(evt){
        if(this.addPartnersDialog){
            this.addPartnersDialog.destroy();
        }
        sap.m.MessageToast.show(model.i18n._getLocaleText("NO_PARTNER_CHOSEN"));

    },


     handleValueHelpPartnerRole : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();

			this.inputPartnerRoleId = oEvent.getSource().getId();
			// create value help dialog
			if (!this._valueHelpPartnerRoleDialog) {
				this._valueHelpPartnerRoleDialog = sap.ui.xmlfragment(
					"view.dialog.valueHelpPartnerRoleDialog",
					this
				);
                var page = this.getView().byId("detailPageId");
//				this.getView().addDependent(this._valueHelpPartnerRoleDialog);
				page.addDependent(this._valueHelpPartnerRoleDialog);
			}
//            var filterEntity = new sap.ui.model.Filter({
//                                    path: "partnerRole",
//                                    operator: "Contains",
//                                    value1: sInputValue
//                                  });
			// create a filter for the binding
			this._valueHelpPartnerRoleDialog.getBinding("items").filter();//[filterEntity]

			// open value help dialog filtered by the input value
			this._valueHelpPartnerRoleDialog.open();//sInputValue
		},

		_handleValueHelpPartnerRoleSearch : function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter1 = new sap.ui.model.Filter({
                                    path: "partnerRole",
                                    operator: "Contains",
                                    value1: sValue
                                  });
            var oFilter2 = new sap.ui.model.Filter({
                                    path: "roleCode",
                                    operator: "Contains",
                                    value1: sValue
                                  });
            var filters = [];
            filters.push(oFilter1);
            filters.push(oFilter2);


            var oFilter = new sap.ui.model.Filter({filters:filters, and:false});
			evt.getSource().getBinding("items").filter([oFilter]);
		},

		_handleValueHelpPartnerRoleClose : function (evt) {
			var oSelectedItem = evt.getParameter("selectedItem");
			if (oSelectedItem) {
                var inputPartner = sap.ui.getCore().byId("partnerInput");
//                var mainPartnerCheckBox = sap.ui.getCore().byId("mainPartnerChecked");
				var input = sap.ui.getCore().byId(this.inputPartnerRoleId);
				input.setValue(oSelectedItem.getTitle());
                inputPartner.setEnabled(true);
//                mainPartnerCheckBox.setEditable(true);
                this.inputPartnerRoleCode = oSelectedItem.getDescription();
                this.inputPartnerRole = oSelectedItem.getTitle();
			}
			evt.getSource().getBinding("items").filter([]);
//            this._valueHelpStatusDialog.destroy();
		},

    ///////******************************************************************//////////
    
     handleValueHelpCategory : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();
 
			this.inputCategoryId = oEvent.getSource().getId();
			// create value help dialog
			if (!this._valueHelpCategoryDialog) {
				this._valueHelpCategoryDialog = sap.ui.xmlfragment(
					"view.dialog.valueHelpCategoryDialog",
					this
				);
				this.getView().addDependent(this._valueHelpCategoryDialog);
			}
//            var filterEntity = new sap.ui.model.Filter({
//                                    path: "categoryName",
//                                    operator: "Contains",
//                                    value1: sInputValue
//                                  });
			// create a filter for the binding
			this._valueHelpCategoryDialog.getBinding("items").filter();
 
			// open value help dialog filtered by the input value
			this._valueHelpCategoryDialog.open();
		},
 
		_handleValueHelpCategorySearch : function (evt) {
			var sValue = evt.getParameter("value");
			
            var oFilter1 = new sap.ui.model.Filter({
                                    path: "categoryName",
                                    operator: "Contains",
                                    value1: sValue
                                  });
            var oFilter2 = new sap.ui.model.Filter({
                                    path: "categoryCode",
                                    operator: "Contains",
                                    value1: sValue
                                  });
            var filters = [];
            filters.push(oFilter1);
            filters.push(oFilter2);


            var oFilter = new sap.ui.model.Filter({filters:filters, and:false});
            
			evt.getSource().getBinding("items").filter([oFilter]);
		},
 
		_handleValueHelpCategoryClose : function (evt) {
			var oSelectedItem = evt.getParameter("selectedItem");
			if (oSelectedItem) {
				var input = sap.ui.getCore().byId(this.inputCategoryId);
				input.setValue(oSelectedItem.getTitle());
                this.ticket.categoryCode = oSelectedItem.getDescription();
			}
			evt.getSource().getBinding("items").filter([]);
//            this._valueHelpPriorityDialog.destroy();
		},


    onLinkToUserInfoPress: function(evt){
      this.router.navTo("changePassword");
    },

    navBackToMobileMaster: function(evt){
      this.router.navTo("masterMobile");
    },



    ////////////**************************************///////////////////////////////////
    ////////////Funzioni che gestiscono gli upload

    openUploadFileDialog: function(evt){
        if(!this.uploadDialog)
        this.uploadDialog = sap.ui.xmlfragment("view.dialog.uploadFile", this);
        var page = this.getView().byId("detailPageId");
        page.addDependent(this.uploadDialog);
        this.uploadDialog.open();
        var fileUploader = sap.ui.getCore().byId("fileUploader");
        fileUploader.clear();
        var descriptionTextArea = sap.ui.getCore().byId("fileDescriptionId");
        descriptionTextArea.setValue("");

        $(document).keyup(_.bind(this.keyUpFunc, this));

    },

    onCloseUploadDialogPress: function(evt){
        if(this.uploadDialog){
            this.uploadDialog.close();
//            this.uploadDialog.destroy();
        }

    },
    
    getFileFromServer: function(evt){
        var src = evt.getSource();
        var position = evt.getSource();
        var oContext = position.getBindingContext("t");
        var file = oContext.getObject();
        var idTicket= file.ticketId;
        
        var fSuccess  = function(result){
            utils.Busy.hide();
            if(result){
                console.log("result ok " + result);
                if(result.results && result.results.length>0){
                    console.log("many results "+result.results);
               
                }
             }else{
                  console.log("Something went wrong");
             }

        };
        fSuccess = _.bind(fSuccess, this);

        var fError  = function(err){
            utils.Busy.hide();
            console.log("Error -- Something went wrong!");

        };
        fError = _.bind(fError, this);

        utils.Busy.show();
      model.odata.chiamateOdata.getFileFromServer(file, fSuccess, fError);
    },

    handleLinkPress: function(evt){
        var position = evt.getSource();
        var oContext = position.getBindingContext("t");
        var oModel = oContext.getModel();
        var row = oContext.getObject();
        var oData = oModel.getData();
        var currentRowIndex = parseInt(oContext.getPath().split('/')[oContext.getPath().split('/').length - 1]);
        var descriptionData = {};
        this.currentFile = oData.docs[currentRowIndex];
        descriptionData.description = oData.docs[currentRowIndex].description;
        this.descriptionModel = new sap.ui.model.json.JSONModel(descriptionData);
        this.getView().setModel(this.descriptionModel, "desc");
        if(!this.descriptionDialog)
        this.descriptionDialog = sap.ui.xmlfragment("view.dialog.fileDescription", this);
        var page = this.getView().byId("detailPageId");
        page.addDependent(this.descriptionDialog);
        this.descriptionDialog.open();

        $(document).keyup(_.bind(this.keyUpFunc, this));
    },

    onCloseDescriptionDialogPress: function(evt){
        if(this.descriptionDialog){
            this.descriptionDialog.close();
//            this.descriptionDialog.destroy();
        }

    },

    onEditDescriptionDialogPress: function(){
        var saveButton = sap.ui.getCore().byId("saveDescriptionButton");
        var cancelButton = sap.ui.getCore().byId("cancelDescriptionButton");
        var editButton = sap.ui.getCore().byId("editDescriptionButton");
        var closeButton = sap.ui.getCore().byId("closeDescriptionButton");
        var textArea = sap.ui.getCore().byId("fileDescriptionText");
        textArea.setEditable(true);
        closeButton.setVisible(false);
        editButton.setVisible(false);
        saveButton.setVisible(true);
        cancelButton.setVisible(true);
    },

    onSaveDescriptionDialogPress: function(evt){
        var s = evt.getSource();
        var parentView = s.getParent().getParent().getParent();
        var saveButton = sap.ui.getCore().byId("saveDescriptionButton");
        var cancelButton = sap.ui.getCore().byId("cancelDescriptionButton");
        var editButton = sap.ui.getCore().byId("editDescriptionButton");
        var closeButton = sap.ui.getCore().byId("closeDescriptionButton");
        var textArea = sap.ui.getCore().byId("fileDescriptionText");
        if(this.currentFile){
            this.currentFile.description = textArea.getValue();
        }
        this.getView(parentView).getModel("t").refresh();
        sap.m.MessageToast.show(model.i18n._getLocaleText("DESCRIPTION_CHANGED"));
        textArea.setEditable(false);
        closeButton.setVisible(true);
        editButton.setVisible(true);
        saveButton.setVisible(false);
        cancelButton.setVisible(false);

    },

    onCancelDescriptionDialogPress: function(){
        var saveButton = sap.ui.getCore().byId("saveDescriptionButton");
        var cancelButton = sap.ui.getCore().byId("cancelDescriptionButton");
        var editButton = sap.ui.getCore().byId("editDescriptionButton");
        var closeButton = sap.ui.getCore().byId("closeDescriptionButton");
        var textArea = sap.ui.getCore().byId("fileDescriptionText");
        textArea.setEditable(false);
        closeButton.setVisible(true);
        editButton.setVisible(true);
        saveButton.setVisible(false);
        cancelButton.setVisible(false);
    },

    onDeleteFilePress: function(evt){
        var position = evt.getParameters().listItem;
        var oContext = position.getBindingContext("t");
        var oModel = oContext.getModel();
        var row = oContext.getObject();
        var oData = oModel.getData();
        var currentRowIndex = parseInt(oContext.getPath().split('/')[oContext.getPath().split('/').length - 1]);
        oData.docs.splice(currentRowIndex,1);
        oModel.updateBindings();
        this.getView().getModel("t").refresh(true);
    },
    
    
    ////*******************************************///////////
    
    handleUploadComplete: function (oEvent) {
    var sResponse = oEvent.getParameter("response");
    var status = oEvent.getParameter("status").toString();
    var dataFile = "";
    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function(searchString, position){
          position = position || 0;
          return this.substr(position, searchString.length) === searchString;
      };
    }
    if (status && status.startsWith("2")) {

//      if (sResponse) {
//        var response = oEvent.getParameter("response");
//        var indexIGuid = response.indexOf("IGuid=");
//        var indexIGuid2 = response.indexOf(",");
//        var IGuid = response.substring(indexIGuid, indexIGuid2).split("'")[1];
//        var indexIDrunr = response.indexOf("IDrunr=");
//        var indexIDrunr2 = response.indexOf(")");
//        var IDrunr = response.substring(indexIDrunr, indexIDrunr2).split("'")[1];
//        this.order.guid = IGuid;
          
        model.collections.Tickets.getById(this.id)
		.then(_.bind(function(res){
            console.log(res);
            this.ticket.setDocs(res.docs);
            this.refreshView(this.ticket);
//            this.setNewTicketStates(res, this.id);

        }, this));
          
//        dataFile = {
//          name: this.fileName,
//          posId: IDrunr,
//          type: this.fileType
//        };
//        var oModel = this.getView().getModel("t");
//        var aEntries = oModel.getData().docs;
//        aEntries.unshift(file);
//        oModel.refresh();

//      }
      sap.m.MessageToast.show(model.i18n._getLocaleText("UPLOAD_OK"));
    } else {
      dataFile = {};
      sap.m.MessageToast.show(model.i18n._getLocaleText("UPLOAD_KO"));
    }
    this.getView().getModel("t").refresh();
    // this.refreshView(this.order);

  },

  
  handleUploadPress: function (oEvent) {

    this.tempFileObject = {
        
    };
    var fileUploader = sap.ui.getCore().byId("fileUploader");
    if (!fileUploader.getValue()) {
      sap.m.MessageToast.show(model.i18n._getLocaleText("CHOOSE_FILE_FIRST"));
      return;
    } else {
      fileUploader.removeAllHeaderParameters();
      fileUploader.setSendXHR(true);

      var modello = model.odata.chiamateOdata.getOdataModel();
      modello.refreshSecurityToken();
      var header = new sap.ui.unified.FileUploaderParameter({
        name: "x-csrf-token",
        value: modello.getHeaders()['x-csrf-token']
      });

      fileUploader.addHeaderParameter(header);
      var file = fileUploader.oFileUpload.files[0];
      this.fileName = file.name;
      var description = sap.ui.getCore().byId("fileDescriptionId").getValue();
      
      file.description = description;
        
          

//      if (this.id) {
        var lastPart = "|" + description;
        var headerSlug = new sap.ui.unified.FileUploaderParameter({
          name: "slug",
          value: this.id + "|" + this.fileName + lastPart
        });
//      } else {
//        var headerSlug = new sap.ui.unified.FileUploaderParameter({
//          name: "slug",
//          value: this.fileName + "|" + this.fileType
//        });
//      }

      fileUploader.addHeaderParameter(headerSlug);

      fileUploader.upload();
    }

    if (this.uploadDialog) {
      this.uploadDialog.close();
      //this.uploadDialog.destroy();
    }
  },
    
    //////*****************************************///////////
    
    
    

//    handleUploadComplete: function(oEvent) {
//			var sResponse = oEvent.getParameter("response");
//			if (sResponse) {
//				var sMsg = "";
//				var m = /^\[(\d\d\d)\]:(.*)$/.exec(sResponse);
//				if (m[1] == "200") {
//					sMsg = "Return Code: " + m[1] + "\n" + m[2], "SUCCESS", "Upload Success";
//					oEvent.getSource().setValue("");
//				} else {
//					sMsg = "Return Code: " + m[1] + "\n" + m[2], "ERROR", "Upload Error";
//				}
//
				//sap.m.MessageToast.show(sMsg);
//
//			}
//            sap.m.MessageToast.show(model.i18n._getLocaleText("UPLOAD_OK"));
//		},

//		handleUploadPress: function(oEvent) {
//
//			var oFileUploader = sap.ui.getCore().byId("fileUploader");
//			if(!oFileUploader.getValue()) {
//				sap.m.MessageToast.show(model.i18n._getLocaleText("CHOOSE_FILE_FIRST"));
//				return;
//			}else{
//                var description = sap.ui.getCore().byId("fileDescriptionId").getValue();
//                var file = oFileUploader.oFileUpload.files[0];
//                file.description = description;
//                var oModel = this.getView().getModel("t");
//                var aEntries = oModel.getData().docs;
//                aEntries.unshift(file);
//                oModel.refresh();
//            }
//
//			oFileUploader.upload();
//            if(this.uploadDialog){
//            this.uploadDialog.close();
//            this.uploadDialog.destroy();
//            }
//		},

		handleTypeMissmatch: function(oEvent) {
			var aFileTypes = oEvent.getSource().getFileType();
			jQuery.each(aFileTypes, function(key, value) {aFileTypes[key] = "*." +  value});
			var sSupportedFileTypes = aFileTypes.join(", ");
			sap.m.MessageToast.show("The file type *." + oEvent.getParameter("fileType") +
									" is not supported. Choose one of the following types: " +
									sSupportedFileTypes);
		},

		handleValueChange: function(oEvent) {
			sap.m.MessageToast.show(model.i18n._getLocaleText("PRESS_UPLOAD_FILE") + " " +
									oEvent.getParameter("newValue") + "'");
		}

//    formatAttribute: function (sValue) {
//    jQuery.sap.require("sap.ui.core.format.FileSizeFormat");
//    if (jQuery.isNumeric(sValue)) {
//      return sap.ui.core.format.FileSizeFormat.getInstance({
//        binaryFilesize: false,
//        maxFractionDigits: 1,
//        maxIntegerDigits: 3
//      }).format(sValue);
//    } else {
//      return sValue;
//    }
//  },
//
//  onChange: function (oEvent) {
//    var oUploadCollection = oEvent.getSource();
//    var filelist = [];
//    filelist.push(oEvent.getParameters().files[0]);
//    // Header Token
//    var oCustomerHeaderToken = new sap.m.UploadCollectionParameter({
//      name: "x-csrf-token",
//      value: "securityTokenFromModel"
//    });
//    oUploadCollection.addHeaderParameter(oCustomerHeaderToken);
//  },
//
//  onFileDeleted: function (oEvent) {
//    var oData = this.getView().byId("UploadCollection").getModel("t").getData();
//    if (oData.docs) {
//      var aItems = jQuery.extend(true, {}, oData).docs;
//    } else {
//      var aItems = [];
//    }
//
//    var sDocumentId = oEvent.getParameter("documentId");
//    jQuery.each(aItems, function (index) {
//      if (aItems[index] && aItems[index].documentId === sDocumentId) {
//        aItems.splice(index, 1);
//      };
//    });
//    this.getView().byId("UploadCollection").getModel("t").refresh();
//    var oUploadCollection = oEvent.getSource();
//    oUploadCollection.setNumberOfAttachmentsText("Uploaded (" + oUploadCollection.getItems().length + ")");
//    sap.m.MessageToast.show("FileDeleted event triggered.");
//  },
//
//  onFileRenamed: function (oEvent) {
//    var oData = this.getView().byId("UploadCollection").getModel().getData();
//    if (oData.docs) {
//      var aItems = jQuery.extend(true, {}, oData).docs;
//    } else {
//      var aItems = [];
//    }
//    var sDocumentId = oEvent.getParameter("documentId");
//    jQuery.each(aItems, function (index) {
//      if (aItems[index] && aItems[index].documentId === sDocumentId) {
//        aItems[index].fileName = oEvent.getParameter("item").getFileName();
//      };
//    });
//    this.getView().byId("UploadCollection").getModel("t").refresh();
//    sap.m.MessageToast.show("FileRenamed event triggered.");
//  },
//
//  onFileSizeExceed: function (oEvent) {
//    sap.m.MessageToast.show("FileSizeExceed event triggered.");
//  },
//
//  onTypeMissmatch: function (oEvent) {
//    sap.m.MessageToast.show("TypeMissmatch event triggered.");
//  },
//
//  onUploadComplete: function (oEvent) {
//    var oData = this.getView().byId("UploadCollection").getModel("t").getData();
//    if (oData.docs) {
//      var aItems = jQuery.extend(true, {}, oData).docs;
//    } else {
//      var aItems = [];
//    }
//    var oItem = {};
//    var sUploadedFile = oEvent.getParameter("files")[0].fileName;
//    // at the moment parameter fileName is not set in IE9
//    if (!sUploadedFile) {
//      var aUploadedFile = (oEvent.getParameters().getSource().getProperty("value")).split(/\" "/);
//      sUploadedFile = aUploadedFile[0];
//    }
//    oItem = {
//      "documentId": jQuery.now().toString(), // generate Id,
//      "fileName": sUploadedFile,
//      "mimeType": "",
//      "thumbnailUrl": "",
//      "url": "",
//      "attributes": [
//        {
//          "title": "Uploaded By",
//          "text": model.persistence.Storage.session.get("user").username,
//						},
//        {
//          "title": "Uploaded On",
//          "text": new Date(jQuery.now()).toLocaleDateString()
//						},
//        {
//          "title": "File Size",
//          "text": "505000"
//						}
//					]
//    };
//    //aItems.unshift(oItem);
//    oData.docs.unshift(oItem);
//    this.getView().byId("UploadCollection").getModel("t").refresh();
//    var oUploadCollection = oEvent.getSource();
//    oUploadCollection.setNumberOfAttachmentsText("Uploaded (" + oUploadCollection.getItems().length + ")");
//    // delay the success message to notice onChange message
//    setTimeout(function () {
//      sap.m.MessageToast.show("UploadComplete event triggered.");
//    }, 4000);
//  },
//
//  onSelectChange: function (oEvent) {
//    var oUploadCollection = sap.ui.getCore().byId(this.getView().getId() + "--UploadCollection");
//    oUploadCollection.setShowSeparators(oEvent.getParameters().selectedItem.getProperty("key"));
//  },
//  onBeforeUploadStarts: function (oEvent) {
//    // Header Slug
//    var oCustomerHeaderSlug = new sap.m.UploadCollectionParameter({
//      name: "slug",
//      value: oEvent.getParameter("fileName")
//    });
//    oEvent.getParameters().addHeaderParameter(oCustomerHeaderSlug);
//    sap.m.MessageToast.show("BeforeUploadStarts event triggered.");
//  },
//  onUploadTerminated: function (oEvent) {
//    // get parameter file name
//    var sFileName = oEvent.getParameter("fileName");
//    // get a header parameter (in case no parameter specified, the callback function getHeaderParameter returns all request headers)
//    var oRequestHeaders = oEvent.getParameters().getHeaderParameter();
//  }


    ///////////******************************************//////////////////////////////


});
