jQuery.sap.require("sap.m.MessageBox");
jQuery.sap.require("utils.Formatter");
jQuery.sap.require("view.abstract.AbstractMasterController");
jQuery.sap.require("model.filters.Filter");
jQuery.sap.require("model.collections.Tickets");
jQuery.sap.require("jquery.sap.storage");
jQuery.sap.require("utils.Collections");

view.abstract.AbstractMasterController.extend("view.TicketList", {

//    _filterDialog:undefined,

	onExit: function () {

	},


	onInit: function () {

		// this.router = sap.ui.core.UIComponent.getRouterFor(this);
		// this.router.attachRoutePatternMatched(this.handleRouteMatched, this);
		view.abstract.AbstractController.prototype.onInit.apply(this, arguments);
		this.uiModel.setProperty("/searchProperty", ["ticketId", "description"]);
        
        

        



	},
    
  


	handleRouteMatched: function (evt) {

		var name = evt.getParameter("name");

        

		if ( name !== "ticketList"
            && name !== "ticketDetail"
            && name!=="noDataSplitDetail"
            && name!=="masterMobile"){
			return;
		}

		this.user = model.persistence.Storage.session.get("user");
        this.appModel = this.getView().getModel("appStatus");
        this.appModel.setProperty("/odataFiltersApplied", false);
        
        if(name==="ticketDetail"){
            return;
        }else{

         this.loadTicketList();
            
        sessionStorage.setItem("filtersAppliedOnMaster", false);
        var eventBus = sap.ui.getCore().getEventBus();
        eventBus.subscribe("loadMasterList", "fireloadMasterList", this.loadTicketList, this);
            
            this.filterOutModel = new sap.ui.model.json.JSONModel();
            var filterTicketId= {"en":false, "results":[{"value": ""}]};
            var filterTicketIdRange={"en":false, "results":{"from":"", "to":""}}
            var filterTicketCreator= {"en":false, "results":[{"value": ""}]};
            var filterMessageProcessor= {"en":false, "results":[{"value": ""}]};
            var filterPriority= {"en":false, "results":[{"value": ""}]};
            var filterStatus= {"en":false, "results":[{"value": ""}]};
            var filterCategory= {"en":false, "results":[{"value": ""}]};

            var now = new Date();
            var past = new Date();
            past.setDate(past.getDate()-30);


            var filterCreationDate= {"en":false, "results":{"from":past, "to":now}};
            var filterlastModifiedDate= {"en":false, "results":{"from":past, "to":now}};




            this.filterOutModel.setData({});

            this.filterOutModel.setProperty("/ticketId", filterTicketId);
            this.filterOutModel.setProperty("/rangeTicket", filterTicketIdRange);
            this.filterOutModel.setProperty("/createdBy", filterTicketCreator);
            this.filterOutModel.setProperty("/messageProcessor", filterMessageProcessor);
            this.filterOutModel.setProperty("/priority", filterPriority);
            this.filterOutModel.setProperty("/status", filterStatus);
            this.filterOutModel.setProperty("/category", filterCategory);
            this.filterOutModel.setProperty("/creationDate", filterCreationDate);
            this.filterOutModel.setProperty("/lastModifiedDate", filterlastModifiedDate);

            this.getView().setModel(this.filterOutModel, "filterOut");
            
        }
        
        this.populateValueHelpDialog();



	},
    
    loadTicketList: function(){
    	var masterView = sap.ui.getCore().byId(this.getView().getId());
    	if(!!masterView)
    	masterView.setBusy(true);
        model.collections.Tickets.loadTickets(this.user)
		 .then(_.bind(this.refreshList, this));
    },


    onItemPress : function(evt)
      {
        var src = evt.getSource();
        var selectedItem = src.getBindingContext("tickets").getObject();
            this.getView().getModel("appStatus").setProperty("/currentSelectedTicket", selectedItem);

            this.router.navTo("ticketDetail",  {id : selectedItem.getId()});


      },
    
    populateValueHelpDialog: function () {
    var pToSelArr = [
      {
        "type": "SSM_Priority",
        "namespace": "priority"
      },
      {
        "type": "SSM_Status_Int",
        "namespace": "statusInt"
      } 
       
    ];
        
//        ,
//      {
//        "type": "SSM_Status",
//        "namespace": "status"
//      } 

    _.map(pToSelArr, _.bind(function (item) {
      utils.Collections.loadOdataSets(item.type)
        .then(_.bind(function (result) {
          this.getView().setModel(result, item.namespace);

        }, this))
    }, this));
  },
    


	refreshList : function(evt)
	{
		var masterView = sap.ui.getCore().byId(this.getView().getId());
		if(!!masterView)
    	masterView.setBusy(false);
		var filters = this.getFiltersOnList();
		this.ticketsModel = model.collections.Tickets.getModel();
		this.getView().setModel(this.ticketsModel, "tickets");


		if(filters)
			this.getView().byId("list").getBinding("items").filter(filters);
		

	},

    onFilterPress:function()
	{
//        var ticketArray = this.getView().byId("list").getBinding("items").oList;
//        var filteredListItems=[];
        var filters = this.getFiltersOnList();
        if(filters && filters.length>0){
            if(filters[0].aFilters && filters[0].aFilters.length >0){
            model.filters.Filter.resetFilter("tickets");
            var list = this.getView().byId("list");
			var binding = list.getBinding("items");
			binding.filter();
            }
        }
//
//            var listItems = this.getView().byId("list").getItems();
//            var pathArray = [];
//            for(var i = 0; i<listItems.length; i++){
//                var currentRowIndex = parseInt(listItems[i].getBindingContextPath().split('/')[listItems[i].getBindingContextPath().split('/').length - 1]);
//                pathArray.push(currentRowIndex);
//            }
//            for(var j = 0; j<pathArray.length; j++){
//                var index = pathArray[j];
//                filteredListItems.push(ticketArray[index]);
//            }
//            this.filterModel = model.filters.Filter.getModel(filteredListItems, "tickets");
//        }else{
            this.filterModel = model.filters.Filter.getModel(this.ticketsModel.getData().tickets, "tickets");
//        }
		this.getView().setModel(this.filterModel, "filter");
		var page = this.getView().byId("ticketListPageId");
		this.filterDialog = sap.ui.xmlfragment("view.dialog.filterDialog", this);
		page.addDependent(this.filterDialog);
		this.filterDialog.open();

	},


	onFilterPropertyPress:function(evt)
	{

		var parentPage = sap.ui.getCore().byId("parent");
		var elementPage = sap.ui.getCore().byId("children");
//		console.log(this.getView().getModel("filter").getData().toString());
		var navCon = sap.ui.getCore().byId("navCon");
		var selectedProp = 	evt.getSource().getBindingContext("filter").getObject();
		this.getView().getModel("filter").setProperty("/selected", selectedProp);
		this.elementListFragment = sap.ui.xmlfragment("view.fragment.FilterList", this);
		elementPage.addContent(this.elementListFragment);

		navCon.to(elementPage, "slide");
		this.getView().getModel("filter").refresh();
	},

	onBackFilterPress:function(evt)
	{
		// this.addSelectedFilterItem();
		this.navConBack();
		this.getView().getModel("filter").setProperty("/selected", "");
		this.elementListFragment.destroy();
	},
	navConBack:function()
	{
		var navCon = sap.ui.getCore().byId("navCon");
		navCon.to(sap.ui.getCore().byId("parent"), "slide");
		this.elementListFragment.destroy();
	},
	afterOpenFilter:function(evt)
	{
		var navCon = sap.ui.getCore().byId("navCon");
		if(navCon.getCurrentPage().getId()== "children")
			navCon.to(sap.ui.getCore().byId("parent"), "slide");
		this.getView().getModel("filter").setProperty("/selected", "");
	},

	onSearchFilter:function(oEvt)
	{
			var aFilters = [];
			var sQuery = oEvt.getSource().getValue();

			if (sQuery && sQuery.length > 0) {


					aFilters.push(this.createFilter(sQuery, "value"));
				}

			// update list binding
			var list = sap.ui.getCore().byId("filterList");
			var binding = list.getBinding("items");
			binding.filter(aFilters);
	},
	createFilter:function(val, property)
	{
		var filter = new sap.ui.model.Filter(property, "EQ", val);
		return filter;
	},
	onFilterDialogClose:function(evt)
	{
//        if (this.gFilters && this.gFilters.length>0)
//            this.gFilters=[];
		if (this.elementListFragment) {this.elementListFragment.destroy();}
		if (this.filterDialog) {
			this.filterDialog.close();
			this.filterDialog.destroy();
		}
	},
	onFilterDialogOK:function(evt)
	{
		var filterItems = model.filters.Filter.getSelectedItems("tickets");
        if(_.isEmpty(filterItems)){
            if(this.elementListFragment)
			this.elementListFragment.destroy();
            this.filterDialog.close();
            this.getView().getModel("filter").setProperty("/selected", "");

            this.filterDialog.destroy();
            delete(this.filterDialog);
        }else{

		if(this.elementListFragment)
			this.elementListFragment.destroy();
		this.filterDialog.close();
		this.getView().getModel("filter").setProperty("/selected", "");
		this.handleFilterConfirm(filterItems);
		this.filterDialog.destroy();
		delete(this.filterDialog);
        }
	},
	handleFilterConfirm: function(selectedItems)
	{
        var finalFilter = [];
        var filters;
//        if (this.gFilters && this.gFilters.length>0){
//            filters = this.gFilters;
//        }else{
            filters = [];
//        }

            _.forEach(selectedItems, _.bind(function(item){
            filters.push(this.createFilter(item.value, item.property));
            },
            this));
        finalFilter.push(new sap.ui.model.Filter({ filters: filters, and: true }));
        var list = this.getView().byId("list");
		var binding = list.getBinding("items");
//        binding.filter(filters);
		binding.filter(finalFilter);
	},
	onResetFilterPress: function()
	{
		model.filters.Filter.resetFilter("tickets");
		// console.log(model.filters.Filter.getSelectedItems("customers"));
        var list = this.getView().byId("list");
        var binding = list.getBinding("items");
        binding.filter();
        if (this.elementListFragment) {
          this.elementListFragment.destroy();
        }
//        if (this.filterDialog) {
//          this.filterDialog.close();
//          this.filterDialog.destroy();
//        }
	},
    
    
    ////////*****************************************************************
    
    
    onAdvancedFilterPress:function(evt)
	{

		var page = this.getView().byId("ticketListPageId");
        this._filterDialog = sap.ui.xmlfragment( "view.dialog.advancedFilter", this );
        page.addDependent( this._filterDialog );
	


		this._filterDialog.open();
	},
    
    onConfirmAdvancedFilterPress: function(evt){
        if ( this._filterDialog ){
            this._filterDialog.close();
            this._filterDialog.destroy();
        }
         this.applyDialogFilter();
    },
    
    onAdvancedFilterCloseButton: function(evt){
        if ( this._filterDialog ){
            this._filterDialog.close();
            this._filterDialog.destroy();
        }
    },




	applyDialogFilter:function()
	{
	    this.odataFilters  = [];


	    if(this.filterOutModel.getProperty("/ticketId/en") && this.filterOutModel.getData().ticketId.results.length > 0)
	    {
	        var filterTicketId = [];
	        for(var i = 0; i<this.filterOutModel.getData().ticketId.results.length; i++)
	        {
	        	if(this.filterOutModel.getData().ticketId.results[i].value !== "")
	        	{
                    var filterItemTicketId =  new sap.ui.model.Filter({
                                    path: "ObjectId",
                                    operator: "EQ",
                                    value1: this.filterOutModel.getData().ticketId.results[i].value
                                  });

		            filterTicketId.push(filterItemTicketId);
	        	}
	        }
	        if(filterTicketId.length>0)
	        {
	        	this.odataFilters.push(new sap.ui.model.Filter({filters:filterTicketId, and:false}));
	        }
	    }
        
        if(this.filterOutModel.getProperty("/rangeTicket/en"))
	    {
	 
	      
	        	if(this.filterOutModel.getData().rangeTicket.results.from !== "" 
                   && this.filterOutModel.getData().rangeTicket.results.to !== "")
	        	{
                    var newTicketRange=  new sap.ui.model.Filter({
                                    path: "ObjectId",
                                    operator: "BT",
                                    value1: this.filterOutModel.getData().rangeTicket.results.from,
                                    value2: this.filterOutModel.getData().rangeTicket.results.to
                                  });
                    this.odataFilters.push(newTicketRange);
	
	        	}
	       
	       
	        	
	       
	    }

	    if(this.filterOutModel.getProperty("/createdBy/en")&& this.filterOutModel.getData().createdBy.results.length > 0)
	    {
	        var filterCreators = [];
	        for(var i = 0; i<this.filterOutModel.getData().createdBy.results.length; i++)
	        {
	        	if(this.filterOutModel.getData().createdBy.results[i].value !== "")
	        	{
                    var filterItemCreator =  new sap.ui.model.Filter({
                                    path: "CreatedBy",
                                    operator: "EQ",
                                    value1: this.filterOutModel.getData().createdBy.results[i].value
                                  });

		            filterCreators.push(filterItemCreator);
	        	}
	        }
	        if(filterCreators.length>0)
	        {
	        	this.odataFilters.push(new sap.ui.model.Filter({filters:filterCreators, and:false}));
	        }
	    }
        
        if(this.filterOutModel.getProperty("/messageProcessor/en")&& this.filterOutModel.getData().messageProcessor.results.length > 0)
	    {
	        var filterMessageProcessor = [];
	        for(var i = 0; i<this.filterOutModel.getData().messageProcessor.results.length; i++)
	        {
	        	if(this.filterOutModel.getData().messageProcessor.results[i].value !== "")
	        	{
                    var filterItem =  new sap.ui.model.Filter({
                                    path: "Bpartner",
                                    operator: "EQ",
                                    value1: this.filterOutModel.getData().messageProcessor.results[i].value
                                  });

		            filterMessageProcessor.push(filterItem);
	        	}
	        }
	        if(filterMessageProcessor.length>0)
	        {
	        	this.odataFilters.push(new sap.ui.model.Filter({filters:filterMessageProcessor, and:false}));
	        }
	    }

	     if(this.filterOutModel.getProperty("/priority/en")&& this.filterOutModel.getData().priority.results.length > 0)
	    {
	        var filterPriorities = [];
	        for(var i = 0; i<this.filterOutModel.getData().priority.results.length; i++)
	        {
	        	if(this.filterOutModel.getData().priority.results[i].value !== "")
	        	{
                    var filterItemPriorities =  new sap.ui.model.Filter({
                                    path: "Priority",
                                    operator: "EQ",
                                    value1: this.filterOutModel.getData().priority.results[i].value
                                  });

		            filterPriorities.push(filterItemPriorities);
	        	}
	        }
	        if(filterPriorities.length > 0)
	        {
	        	this.odataFilters.push(new sap.ui.model.Filter({filters:filterPriorities, and:false}));
	        }
	    }

         if(this.filterOutModel.getProperty("/status/en")&& this.filterOutModel.getData().status.results.length > 0)
	    {
	        var filterStatuses = [];
	        for(var i = 0; i<this.filterOutModel.getData().status.results.length; i++)
	        {
	        	if(this.filterOutModel.getData().status.results[i].value !== "")
	        	{
                    var filterItemStatuses =  new sap.ui.model.Filter({
                                    path: "Status",
                                    operator: "EQ",
                                    value1: this.filterOutModel.getData().status.results[i].value
                                  });

		            filterStatuses.push(filterItemStatuses);
	        	}
	        }
	        if(filterStatuses.length > 0)
	        {
	        	this.odataFilters.push(new sap.ui.model.Filter({filters:filterStatuses, and:false}));
	        }
	    }
        
        if(this.filterOutModel.getProperty("/category/en")&& this.filterOutModel.getData().category.results.length > 0)
	    {
	        var filterCategories = [];
	        for(var i = 0; i<this.filterOutModel.getData().category.results.length; i++)
	        {
	        	if(this.filterOutModel.getData().category.results[i].value !== "")
	        	{
                    var filterItemCategory =  new sap.ui.model.Filter({
                                    path: "Category",
                                    operator: "EQ",
                                    value1: this.filterOutModel.getData().category.results[i].value
                                  });

		            filterCategories.push(filterItemCategory);
	        	}
	        }
	        if(filterCategories.length > 0)
	        {
	        	this.odataFilters.push(new sap.ui.model.Filter({filters:filterCategories, and:false}));
	        }
	    }
        

        
        if(this.filterOutModel.getProperty("/creationDate/en"))
	    {
	 
	      
	        	if(this.filterOutModel.getData().creationDate.results.from !== "" 
                   && this.filterOutModel.getData().creationDate.results.to !== "")
	        	{
		            var newDateFilter=  new sap.ui.model.Filter({
                                    path: "PostingDate",
                                    operator: "BT",
                                    value1: this.filterOutModel.getData().creationDate.results.from,
                                    value2: this.filterOutModel.getData().creationDate.results.to
                                  });

		       
                    this.odataFilters.push(newDateFilter);
	        	}
	       
	       
	        	
	       
	    }
        
        if(this.filterOutModel.getProperty("/lastModifiedDate/en"))
	    {
	 
	      
	        	if(this.filterOutModel.getData().lastModifiedDate.results.from !== "" 
                   && this.filterOutModel.getData().lastModifiedDate.results.to !== "")
	        	{
		            var newDateFilter=  new sap.ui.model.Filter({
                                    path: "ChangeDate",
                                    operator: "BT",
                                    value1: this.filterOutModel.getData().lastModifiedDate.results.from,
                                    value2: this.filterOutModel.getData().lastModifiedDate.results.to
                                  });

		       
          
	        	}
	       
	       
	        	this.odataFilters.push(newDateFilter);
	       
	    }

        
        var userFilter = new sap.ui.model.Filter({
                                    path: "Uname",
                                    operator: "EQ",
                                    value1: this.user.username
                                  });
        this.odataFilters.push(userFilter);
        
        
        
        //////////////////*********************************************************////////////////////
        ////////////TODO fare la chiamata all'odata inserendo questi filtri///////////////////////////
        
         this.loadFilteredTickets();
        sessionStorage.setItem("filtersAppliedOnMaster", true);
        var eventBus = sap.ui.getCore().getEventBus();
        eventBus.subscribe("loadFilteredMasterList", "fireloadFilteredMasterList", this.loadFilteredTickets, this);


	},
    
    loadFilteredTickets: function(){
        model.collections.Tickets.loadFilteredTickets(this.odataFilters)
		 .then(_.bind(this.refreshList, this));
        this.appModel.setProperty("/odataFiltersApplied", true);
    },
    
    restoreFilters: function(evt){
//        this.appModel.setProperty("/odataFiltersApplied", false);
//        var device = this.getView().getModel("device").getData();
        this.odataFilters = [];
        sessionStorage.setItem("filtersAppliedOnMaster", false);
        var isPhone = sap.ui.Device.system.phone;
        if(!isPhone){
	          this.router.stop();
	          this.router.initialize(); 
	          this.router.navTo("noDataSplitDetail");
	      }else{
	    	  this.loadTicketList();
	      }
//        model.collections.Tickets.loadTickets(this.user)
//		 .then(_.bind(function () {
//            this.refreshList();
//            if(!isPhone){
//                this.router.stop();
//                this.router.initialize(); 
//                this.router.navTo("noDataSplitDetail");
//            }
//                    
//            }, this));
        var filterTicketId= {"en":false, "results":[{"value": ""}]};
        var filterTicketIdRange={"en":false, "results":{"from":"", "to":""}}
        var filterTicketCreator= {"en":false, "results":[{"value": ""}]};
        var filterMessageProcessor= {"en":false, "results":[{"value": ""}]};
        var filterPriority= {"en":false, "results":[{"value": ""}]};
        var filterStatus= {"en":false, "results":[{"value": ""}]};
        var filterCategory= {"en":false, "results":[{"value": ""}]};

        var now = new Date();
        var past = new Date();
        past.setDate(past.getDate()-30);

      
        var filterCreationDate= {"en":false, "results":{"from":past, "to":now}};
        var filterlastModifiedDate= {"en":false, "results":{"from":past, "to":now}};

        this.filterOutModel.setData({});

        this.filterOutModel.setProperty("/ticketId", filterTicketId);
        this.filterOutModel.setProperty("/rangeTicket", filterTicketIdRange);
        this.filterOutModel.setProperty("/createdBy", filterTicketCreator);
        this.filterOutModel.setProperty("/messageProcessor", filterMessageProcessor);
        this.filterOutModel.setProperty("/priority", filterPriority);
        this.filterOutModel.setProperty("/status", filterStatus);
        this.filterOutModel.setProperty("/category", filterCategory);
        this.filterOutModel.setProperty("/creationDate", filterCreationDate);
        this.filterOutModel.setProperty("/lastModifiedDate", filterlastModifiedDate);

        this.getView().setModel(this.filterOutModel, "filterOut");

    },
    

	onAddFilterRow:function(evt)
  {
      var src = evt.getSource();
   
    var list = src.getParent().getParent().getParent().getParent().getContent()[0].getItems()[0].getItems()[0];
      var idList = list.getId();
        idList =  idList.substring(0, idList.length - 4);
      this._addOutputRowFilterItem(idList);

  },

  activateEnable : function(evt)
  {
  	var src = evt.getSource();
  	if(src.getExpanded())
  	{
  		var idPanel = src.getId();
	  	var parameters = ["ticketId","messageProcessor","lastModifiedDate","creationDate","rangeTicket", "createdBy", "priority", "status","category"];
	  	for(var i = 0; i< parameters.length; i++)
	  	{
	  		if(idPanel.indexOf(parameters[i])>-1)
	  		{
	  			var property = "/"+parameters[i]+"/en";
			  	this.filterOutModel.setProperty(property, true);
			  	this.filterOutModel.refresh();
			  	break;
	  		}
	  	}
  	}


  },


  handleDelete: function(oEvent) {
    var oList = oEvent.getSource();
    var idList = oList.getId();
    idList =  idList.substring(0, idList.length - 4);
    var  oItem = oEvent.getParameter("listItem");
    var  sObj = oItem.getBindingContext("filterOut").getObject();
    var arrayPath = "/"+idList+"/results";
    var arrayData = this.filterOutModel.getProperty(arrayPath);
    if(arrayData.length > 1)
    {
        var delObj = _.remove(arrayData, {value:sObj.value});
        if(arrayData.length <1 )
        {
            var emptyObj = {"value":""};
            this.filterOutModel.setProperty(arrayPath+"/0", emptyObj);
        }
        this.filterOutModel.setProperty(arrayPath, arrayData);
    }
    else
    {
        var emptyObj = {"value":""};
        this.filterOutModel.setProperty(arrayPath+"/0", emptyObj);
    }
    this.filterOutModel.refresh();

  },



  _addOutputRowFilterItem:function(type)
  {
      var typeArrayLength = this.filterOutModel.getProperty("/"+type+"/results").length;
      var emptyElementPath = "/"+type+"/results/"+typeArrayLength;
      var emptyElementData= {
          "value":""
         
      };
      this.filterOutModel.setProperty(emptyElementPath, emptyElementData);
      this.filterOutModel.refresh();
  },

    
    
    handleValueHelpPriority : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();
 
			this.inputPriorityId = oEvent.getSource().getId();
			// create value help dialog
			if (!this._valueHelpPriorityDialog) {
				this._valueHelpPriorityDialog = sap.ui.xmlfragment(
					"view.dialog.valueHelpPriorityDialog",
					this
				);
				this.getView().addDependent(this._valueHelpPriorityDialog);
			}
            var filterEntity = new sap.ui.model.Filter({
                                    path: "priorityTxt",
                                    operator: "Contains",
                                    value1: sInputValue
                                  });
			// create a filter for the binding
			this._valueHelpPriorityDialog.getBinding("items").filter([filterEntity]);
 
			// open value help dialog filtered by the input value
			this._valueHelpPriorityDialog.open(sInputValue);
		},
 
		_handleValueHelpPrioritySearch : function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter = new sap.ui.model.Filter({
                                    path: "priorityTxt",
                                    operator: "Contains",
                                    value1: sValue
                                  });
			evt.getSource().getBinding("items").filter([oFilter]);
		},
 
		_handleValueHelpPriorityClose : function (evt) {
			var oSelectedItem = evt.getParameter("selectedItem");
			if (oSelectedItem) {
				var input = sap.ui.getCore().byId(this.inputPriorityId);
				input.setValue(oSelectedItem.getTitle());
			}
			evt.getSource().getBinding("items").filter([]);
//            this._valueHelpPriorityDialog.destroy();
		},
    
    handleValueHelpStatus : function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();
 
			this.inputStatusId = oEvent.getSource().getId();
			// create value help dialog
			if (!this._valueHelpStatusDialog) {
				this._valueHelpStatusDialog = sap.ui.xmlfragment(
					"view.dialog.valueHelpStatusDialog",
					this
				);
				this.getView().addDependent(this._valueHelpStatusDialog);
			}
            var filterEntity = new sap.ui.model.Filter({
                                    path: "statusText",
                                    operator: "Contains",
                                    value1: sInputValue
                                  });
			// create a filter for the binding
			this._valueHelpStatusDialog.getBinding("items").filter([filterEntity]);
 
			// open value help dialog filtered by the input value
			this._valueHelpStatusDialog.open(sInputValue);
		},
 
		_handleValueHelpStatusSearch : function (evt) {
			var sValue = evt.getParameter("value");
			var oFilter = new sap.ui.model.Filter({
                                    path: "statusText",
                                    operator: "Contains",
                                    value1: sValue
                                  });
			evt.getSource().getBinding("items").filter([oFilter]);
		},
 
		_handleValueHelpStatusClose : function (evt) {
			var oSelectedItem = evt.getParameter("selectedItem");
			if (oSelectedItem) {
				var input = sap.ui.getCore().byId(this.inputStatusId);
				input.setValue(oSelectedItem.getDescription());
			}
			evt.getSource().getBinding("items").filter([]);
//            this._valueHelpStatusDialog.destroy();
		}











});
